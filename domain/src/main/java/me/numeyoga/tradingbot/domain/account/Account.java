package me.numeyoga.tradingbot.domain.account;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
public class Account {
    private String id;
    private String alias;
    private String currency;
    private BigDecimal balance;
}
