package me.numeyoga.tradingbot.domain;

import lombok.NoArgsConstructor;

import javax.enterprise.context.Dependent;
import java.util.Objects;

@Dependent
@NoArgsConstructor
public class SundayIsntFriday {

    String today;

    public void setToday(String today) {
        this.today = today;
    }

    public boolean isChosenDay(String chosenDay) {
        return Objects.equals(today, chosenDay);
    }

    public String getText(boolean todayIsChosenDay) {
        return todayIsChosenDay ? "Yes" : "Nope";
    }
}
