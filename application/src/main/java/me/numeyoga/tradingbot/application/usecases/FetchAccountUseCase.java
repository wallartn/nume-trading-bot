package me.numeyoga.tradingbot.application.usecases;

import me.numeyoga.tradingbot.domain.account.Account;

public interface FetchAccountUseCase {

    Account getAccount(AccountCommand command);

    record AccountCommand(String token, String accountId) {
    }
}
