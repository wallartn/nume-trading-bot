package me.numeyoga.tradingbot.application.ports.output;

import me.numeyoga.tradingbot.domain.account.Account;

public interface FetchAccountOutputPort {

    Account fetchAccount(String id);
}
