package me.numeyoga.tradingbot.application.ports.input;

import me.numeyoga.tradingbot.application.ports.output.FetchAccountOutputPort;
import me.numeyoga.tradingbot.application.usecases.FetchAccountUseCase;
import me.numeyoga.tradingbot.domain.account.Account;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class FetchAccountInputPort implements FetchAccountUseCase {

    FetchAccountOutputPort fetchAccountOutputPort;

    @Override
    public Account getAccount(AccountCommand command) {
        return fetchAccountOutputPort.fetchAccount(command.accountId());
    }
}
