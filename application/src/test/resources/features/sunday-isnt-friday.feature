Feature: Is it Friday yet?
  Everybody wants to know when it's Friday

  Scenario: Sunday isn't Friday
    Given Today is sunday
    When I ask whether it's chosen day yet
    | Day    | Result |
    | Sunday | true   |
    | Monday | false  |
    | Friday | false  |
    Then I should be told
      | Day    | Result |
      | Sunday | Yes    |
      | Monday | Nope   |
      | Friday | Nope   |