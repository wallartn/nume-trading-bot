package me.numeyoga.tradingbot.application;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.quarkus.test.junit.QuarkusTest;
import me.numeyoga.tradingbot.domain.SundayIsntFriday;
import org.jboss.logging.Logger;
import org.junit.jupiter.api.Assertions;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@QuarkusTest
public class SundayIsntFridayTest {

    @Inject
    Logger logger;

    SundayIsntFriday sut = new SundayIsntFriday();

    @Given("Today is sunday")
    public void today_is_sunday() throws Exception {
        sut.setToday("Sunday");
    }

    @When("I ask whether it's chosen day yet")
    public void i_ask_whether_it_s_friday_yet(DataTable table) throws Exception {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            logger.info(String.format("Day %s , Result %s", row.get("Day"), row.get("Result")));
            Assertions.assertEquals(Boolean.parseBoolean(row.get("Result")), sut.isChosenDay(row.get("Day")));
        }
    }

    @Then("I should be told")
    public void i_should_be_told(DataTable table) throws Exception {
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> row : rows) {
            Assertions.assertEquals(row.get("Result"), sut.getText(sut.isChosenDay(row.get("Day"))));
        }
    }

}
