package me.numeyoga.tradingbot.application;

import io.quarkiverse.cucumber.CucumberOptions;
import io.quarkiverse.cucumber.CucumberQuarkusTest;
import io.quarkus.test.junit.QuarkusTest;

/**
 * This Quarkus test class will bootstrap Cucumber and allow the features and steps to be found.
 * [Quarkus Cucumber](https://quarkiverse.github.io/quarkiverse-docs/quarkus-cucumber/dev/index.html)
 */
@QuarkusTest
@CucumberOptions(glue = {"me.numeyoga.tradingbot.application"}, publish = false, plugin = {"pretty", "html:target/cucumber-reports/Cucumber.html"}, features = {"classpath:features"})
public class BootstrapCucumberTests extends CucumberQuarkusTest {
    public static void main(String[] args) {
        // This main method is needed to launch the test from the IDE.
        CucumberQuarkusTest.runMain(BootstrapCucumberTests.class, args);
    }
}
