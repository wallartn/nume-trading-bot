package me.numeyoga.tradingbot.framework.adapters.output.service;

import io.quarkus.test.Mock;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountGetResponse;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountListResponse;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.json.bind.JsonbBuilder;
import java.io.InputStream;

@Mock
@RestClient
public class AccountsServiceMock implements AccountsService {

    @Override
    public AccountGetResponse getById(String id) {
        InputStream inputStream = this.getClass().getResourceAsStream("/json/account.json");
        return JsonbBuilder.create().fromJson(inputStream, AccountGetResponse.class);
    }

    @Override
    public AccountListResponse getAll() {
        InputStream inputStream = this.getClass().getResourceAsStream("/json/accounts.json");
        return JsonbBuilder.create().fromJson(inputStream, AccountListResponse.class);
    }
}