package me.numeyoga.tradingbot.framework;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import me.numeyoga.tradingbot.framework.adapters.output.FetchAccountAdapter;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.OandaConfig;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@TestHTTPEndpoint(FetchAccountAdapter.class)
class FetchAccountTest {

    @Inject
    OandaConfig config;

    @Test
    void fetchConfiguredAccount() {
        given().log().all()
                .when().get("/" + config.accountId())
                .then()
                .statusCode(200)
                .body("id", is(config.accountId()));
    }

    @Test
    void fetchAllAccounts() {
        given()
                .log().all()
                .when().get()
                .then()
                .statusCode(200)
                .body("accounts.id", hasItems(config.accountId()));
    }
}
