package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@QuarkusTest
class AccountListResponseTest {

    @Test
    void deserializeAccountsWithOneAccount() {
        String json = """
                {
                	"accounts": [
                		{
                			"id": "101-004-20509834-001",
                			"tags": []
                		}
                	]
                }
                """;
        Jsonb jsonb = JsonbBuilder.create();
        AccountListResponse response = jsonb.fromJson(json, AccountListResponse.class);
        assertEquals(1, response.getAccounts().size());
        AccountProperties account = response.getAccounts().get(0);
        assertEquals("101-004-20509834-001", account.getId().toString());
        assertEquals(0, account.getTags().size());
        assertNull(account.getMt4AccountID());
    }

    @Test
    void deserializeAccountsWithTwoAccount() {
        String json = """
                {
                	"accounts": [
                		{
                			"id": "101-004-20509834-001",
                			"tags": []
                		},
                		{
                			"id": "101-004-20509834-002",
                			"tags": []
                		}
                	]
                }
                """;
        Jsonb jsonb = JsonbBuilder.create();
        AccountListResponse response = jsonb.fromJson(json, AccountListResponse.class);
        assertEquals(2, response.getAccounts().size());
        checkAccount(response, 0, "101-004-20509834-001");
        checkAccount(response, 1, "101-004-20509834-002");
    }

    private void checkAccount(AccountListResponse response, int i, String expected) {
        AccountProperties account = response.getAccounts().get(i);
        assertEquals(expected, account.getId().toString());
        assertEquals(0, account.getTags().size());
        assertNull(account.getMt4AccountID());
    }

}