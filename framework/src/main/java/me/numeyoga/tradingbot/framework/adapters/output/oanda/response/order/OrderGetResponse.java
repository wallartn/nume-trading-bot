package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * OrderGetResponse
 */
public class OrderGetResponse {

    /**
     * OrderGetResponse Constructor
     * <p>
     * Construct a new OrderGetResponse
     */
    private OrderGetResponse() {
    }

    @JsonbProperty("order") private Order order;

    /**
     * Get the order
     * <p>
     * The details of the Order requested
     * <p>
     * @return the order
     * @see Order
     */
    public Order getOrder() {
        return this.order;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
