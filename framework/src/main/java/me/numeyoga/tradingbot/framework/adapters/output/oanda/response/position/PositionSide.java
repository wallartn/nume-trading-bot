package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.position;

import lombok.NoArgsConstructor;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common.PriceValue;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.AccountUnits;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.DecimalNumber;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade.TradeID;

import javax.json.bind.annotation.JsonbProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The representation of a Position for a single direction (long or short).
 * <p>
 * {units} @ {averagePrice}, {pl} PL {unrealizedPL} UPL
 */
@NoArgsConstructor
public class PositionSide {

    @JsonbProperty("units")
    DecimalNumber units;
    @JsonbProperty("averagePrice")
    PriceValue averagePrice;
    @JsonbProperty("tradeIDs")
    List<TradeID> tradeIDs;
    @JsonbProperty("pl")
    AccountUnits pl;
    @JsonbProperty("unrealizedPL")
    AccountUnits unrealizedPL;
    @JsonbProperty("resettablePL")
    AccountUnits resettablePL;
    @JsonbProperty("financing")
    AccountUnits financing;
    @JsonbProperty("guaranteedExecutionFees")
    AccountUnits guaranteedExecutionFees;

    /**
     * Copy constructor
     * <p>
     *
     * @param other the PositionSide to copy
     */
    public PositionSide(PositionSide other) {
        this.units = other.units;
        this.averagePrice = other.averagePrice;
        if (other.tradeIDs != null) {
            this.tradeIDs = new ArrayList<>(other.tradeIDs);
        }
        this.pl = other.pl;
        this.unrealizedPL = other.unrealizedPL;
        this.resettablePL = other.resettablePL;
        this.financing = other.financing;
        this.guaranteedExecutionFees = other.guaranteedExecutionFees;
    }

    /**
     * Get the Units
     * <p>
     * Number of units in the position (negative value indicates short
     * position, positive indicates long position).
     * <p>
     *
     * @return the Units
     * @see DecimalNumber
     */
    public DecimalNumber getUnits() {
        return this.units;
    }

    /**
     * Set the Units
     * <p>
     * Number of units in the position (negative value indicates short
     * position, positive indicates long position).
     * <p>
     *
     * @param units the Units as a DecimalNumber
     * @return {@link PositionSide PositionSide}
     * @see DecimalNumber
     */
    public PositionSide setUnits(DecimalNumber units) {
        this.units = units;
        return this;
    }

    /**
     * Get the Average Price
     * <p>
     * Volume-weighted average of the underlying Trade open prices for the
     * Position.
     * <p>
     *
     * @return the Average Price
     * @see PriceValue
     */
    public PriceValue getAveragePrice() {
        return this.averagePrice;
    }

    /**
     * Set the Average Price
     * <p>
     * Volume-weighted average of the underlying Trade open prices for the
     * Position.
     * <p>
     *
     * @param averagePrice the Average Price as a PriceValue
     * @return {@link PositionSide PositionSide}
     * @see PriceValue
     */
    public PositionSide setAveragePrice(PriceValue averagePrice) {
        this.averagePrice = averagePrice;
        return this;
    }

    /**
     * Get the Trade IDs
     * <p>
     * List of the open Trade IDs which contribute to the open Position.
     * <p>
     *
     * @return the Trade IDs
     * @see TradeID
     */
    public List<TradeID> getTradeIDs() {
        return this.tradeIDs;
    }

    /**
     * Set the Trade IDs
     * <p>
     * List of the open Trade IDs which contribute to the open Position.
     * <p>
     *
     * @param tradeIDs the Trade IDs
     * @return {@link PositionSide PositionSide}
     * @see TradeID
     */
    public PositionSide setTradeIDs(List<TradeID> tradeIDs) {
        this.tradeIDs = tradeIDs;
        return this;
    }

    /**
     * Get the Profit/Loss
     * <p>
     * Profit/loss realized by the PositionSide over the lifetime of the
     * Account.
     * <p>
     *
     * @return the Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getPl() {
        return this.pl;
    }

    /**
     * Set the Profit/Loss
     * <p>
     * Profit/loss realized by the PositionSide over the lifetime of the
     * Account.
     * <p>
     *
     * @param pl the Profit/Loss as an AccountUnits
     * @return {@link PositionSide PositionSide}
     * @see AccountUnits
     */
    public PositionSide setPl(AccountUnits pl) {
        this.pl = pl;
        return this;
    }

    /**
     * Get the Unrealized Profit/Loss
     * <p>
     * The unrealized profit/loss of all open Trades that contribute to this
     * PositionSide.
     * <p>
     *
     * @return the Unrealized Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getUnrealizedPL() {
        return this.unrealizedPL;
    }

    /**
     * Set the Unrealized Profit/Loss
     * <p>
     * The unrealized profit/loss of all open Trades that contribute to this
     * PositionSide.
     * <p>
     *
     * @param unrealizedPL the Unrealized Profit/Loss as an AccountUnits
     * @return {@link PositionSide PositionSide}
     * @see AccountUnits
     */
    public PositionSide setUnrealizedPL(AccountUnits unrealizedPL) {
        this.unrealizedPL = unrealizedPL;
        return this;
    }

    /**
     * Get the Resettable Profit/Loss
     * <p>
     * Profit/loss realized by the PositionSide since the Account's
     * resettablePL was last reset by the client.
     * <p>
     *
     * @return the Resettable Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getResettablePL() {
        return this.resettablePL;
    }

    /**
     * Set the Resettable Profit/Loss
     * <p>
     * Profit/loss realized by the PositionSide since the Account's
     * resettablePL was last reset by the client.
     * <p>
     *
     * @param resettablePL the Resettable Profit/Loss as an AccountUnits
     * @return {@link PositionSide PositionSide}
     * @see AccountUnits
     */
    public PositionSide setResettablePL(AccountUnits resettablePL) {
        this.resettablePL = resettablePL;
        return this;
    }

    /**
     * Get the Financing
     * <p>
     * The total amount of financing paid/collected for this PositionSide over
     * the lifetime of the Account.
     * <p>
     *
     * @return the Financing
     * @see AccountUnits
     */
    public AccountUnits getFinancing() {
        return this.financing;
    }

    /**
     * Set the Financing
     * <p>
     * The total amount of financing paid/collected for this PositionSide over
     * the lifetime of the Account.
     * <p>
     *
     * @param financing the Financing as an AccountUnits
     * @return {@link PositionSide PositionSide}
     * @see AccountUnits
     */
    public PositionSide setFinancing(AccountUnits financing) {
        this.financing = financing;
        return this;
    }

    /**
     * Get the Guranteed Execution Fees
     * <p>
     * The total amount of fees charged over the lifetime of the Account for
     * the execution of guaranteed Stop Loss Orders attached to Trades for this
     * PositionSide.
     * <p>
     *
     * @return the Guranteed Execution Fees
     * @see AccountUnits
     */
    public AccountUnits getGuaranteedExecutionFees() {
        return this.guaranteedExecutionFees;
    }

    /**
     * Set the Guranteed Execution Fees
     * <p>
     * The total amount of fees charged over the lifetime of the Account for
     * the execution of guaranteed Stop Loss Orders attached to Trades for this
     * PositionSide.
     * <p>
     *
     * @param guaranteedExecutionFees the Guranteed Execution Fees as an
     *                                AccountUnits
     * @return {@link PositionSide PositionSide}
     * @see AccountUnits
     */
    public PositionSide setGuaranteedExecutionFees(AccountUnits guaranteedExecutionFees) {
        this.guaranteedExecutionFees = guaranteedExecutionFees;
        return this;
    }

    @Override
    public String toString() {
        return "PositionSide(" +
                "units=" +
                (units == null ? "null" : units.toString()) + ", " +
                "averagePrice=" +
                (averagePrice == null ? "null" : averagePrice.toString()) + ", " +
                "tradeIDs=" +
                (tradeIDs == null ? "null" : tradeIDs.toString()) + ", " +
                "pl=" +
                (pl == null ? "null" : pl.toString()) + ", " +
                "unrealizedPL=" +
                (unrealizedPL == null ? "null" : unrealizedPL.toString()) + ", " +
                "resettablePL=" +
                (resettablePL == null ? "null" : resettablePL.toString()) + ", " +
                "financing=" +
                (financing == null ? "null" : financing.toString()) + ", " +
                "guaranteedExecutionFees=" +
                (guaranteedExecutionFees == null ? "null" : guaranteedExecutionFees.toString()) +
                ")";
    }
}
