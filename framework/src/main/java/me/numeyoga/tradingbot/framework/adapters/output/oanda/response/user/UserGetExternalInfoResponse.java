package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.user;

import javax.json.bind.annotation.JsonbProperty;

/**
 * UserGetExternalInfoResponse
 */
public class UserGetExternalInfoResponse {

    @JsonbProperty("userInfo")
    private UserInfoExternal userInfo;

    /**
     * UserGetExternalInfoResponse Constructor
     * <p>
     * Construct a new UserGetExternalInfoResponse
     */
    private UserGetExternalInfoResponse() {
    }

    /**
     * Get the userInfo
     * <p>
     * The user information for the specifier user.
     * <p>
     *
     * @return the userInfo
     * @see UserInfoExternal
     */
    public UserInfoExternal getUserInfo() {
        return this.userInfo;
    }
}
