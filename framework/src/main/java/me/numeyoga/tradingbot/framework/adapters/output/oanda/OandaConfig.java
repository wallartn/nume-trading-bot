package me.numeyoga.tradingbot.framework.adapters.output.oanda;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithName;

@ConfigMapping(prefix = "config.oanda")
public interface OandaConfig {

    @WithName("api-url")
    String apiUrl();
    @WithName("bearer-token")
    String bearerToken();
    @WithName("account-id")
    String accountId();
    @WithName("account-type")
    String accountType();

}
