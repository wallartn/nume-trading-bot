package me.numeyoga.tradingbot.framework.adapters.output.oanda.request;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.OandaConfig;
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class OandaBlockingClientHeadersFactory implements ClientHeadersFactory {

    @Inject
    OandaConfig config;

    @Override
    public MultivaluedMap<String, String> update(MultivaluedMap<String, String> incomingHeaders, MultivaluedMap<String, String> clientOutgoingHeaders) {
        clientOutgoingHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + config.bearerToken());
        return clientOutgoingHeaders;
    }
}
