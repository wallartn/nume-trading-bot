package me.numeyoga.tradingbot.framework.adapters.output.mapper;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.Account;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AccountMapper {


    public me.numeyoga.tradingbot.domain.account.Account toDomain(Account account) {
        me.numeyoga.tradingbot.domain.account.Account domain = new me.numeyoga.tradingbot.domain.account.Account();
        domain.setId(account.getId().toString());
        domain.setAlias(account.getAlias());
        domain.setCurrency(account.getCurrency().toString());
        domain.setBalance(account.getBalance().bigDecimalValue());
        return domain;
    }
}
