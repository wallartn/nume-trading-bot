package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.RequestException;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.OrderCancelRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.StopLossOrderRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TakeProfitOrderRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TrailingStopLossOrderRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * TradeSetDependentOrders400RequestException
 */
public class TradeSetDependentOrders400RequestException extends RequestException {

    private static final long serialVersionUID = 379194315785145496L;

    /**
     * TradeSetDependentOrders400RequestException Constructor
     * <p>
     * Construct a new TradeSetDependentOrders400RequestException
     */
    private TradeSetDependentOrders400RequestException() {
    }

    @JsonbProperty("takeProfitOrderCancelRejectTransaction") private OrderCancelRejectTransaction takeProfitOrderCancelRejectTransaction;

    /**
     * Get the takeProfitOrderCancelRejectTransaction
     * <p>
     * An OrderCancelRejectTransaction represents the rejection of the
     * cancellation of an Order in the client's Account.
     * <p>
     * @return the takeProfitOrderCancelRejectTransaction
     * @see OrderCancelRejectTransaction
     */
    public OrderCancelRejectTransaction getTakeProfitOrderCancelRejectTransaction() {
        return this.takeProfitOrderCancelRejectTransaction;
    }

    @JsonbProperty("takeProfitOrderRejectTransaction") private TakeProfitOrderRejectTransaction takeProfitOrderRejectTransaction;

    /**
     * Get the takeProfitOrderRejectTransaction
     * <p>
     * A TakeProfitOrderRejectTransaction represents the rejection of the
     * creation of a TakeProfit Order.
     * <p>
     * @return the takeProfitOrderRejectTransaction
     * @see TakeProfitOrderRejectTransaction
     */
    public TakeProfitOrderRejectTransaction getTakeProfitOrderRejectTransaction() {
        return this.takeProfitOrderRejectTransaction;
    }

    @JsonbProperty("stopLossOrderCancelRejectTransaction") private OrderCancelRejectTransaction stopLossOrderCancelRejectTransaction;

    /**
     * Get the stopLossOrderCancelRejectTransaction
     * <p>
     * An OrderCancelRejectTransaction represents the rejection of the
     * cancellation of an Order in the client's Account.
     * <p>
     * @return the stopLossOrderCancelRejectTransaction
     * @see OrderCancelRejectTransaction
     */
    public OrderCancelRejectTransaction getStopLossOrderCancelRejectTransaction() {
        return this.stopLossOrderCancelRejectTransaction;
    }

    @JsonbProperty("stopLossOrderRejectTransaction") private StopLossOrderRejectTransaction stopLossOrderRejectTransaction;

    /**
     * Get the stopLossOrderRejectTransaction
     * <p>
     * A StopLossOrderRejectTransaction represents the rejection of the
     * creation of a StopLoss Order.
     * <p>
     * @return the stopLossOrderRejectTransaction
     * @see StopLossOrderRejectTransaction
     */
    public StopLossOrderRejectTransaction getStopLossOrderRejectTransaction() {
        return this.stopLossOrderRejectTransaction;
    }

    @JsonbProperty("trailingStopLossOrderCancelRejectTransaction") private OrderCancelRejectTransaction trailingStopLossOrderCancelRejectTransaction;

    /**
     * Get the trailingStopLossOrderCancelRejectTransaction
     * <p>
     * An OrderCancelRejectTransaction represents the rejection of the
     * cancellation of an Order in the client's Account.
     * <p>
     * @return the trailingStopLossOrderCancelRejectTransaction
     * @see OrderCancelRejectTransaction
     */
    public OrderCancelRejectTransaction getTrailingStopLossOrderCancelRejectTransaction() {
        return this.trailingStopLossOrderCancelRejectTransaction;
    }

    @JsonbProperty("trailingStopLossOrderRejectTransaction") private TrailingStopLossOrderRejectTransaction trailingStopLossOrderRejectTransaction;

    /**
     * Get the trailingStopLossOrderRejectTransaction
     * <p>
     * A TrailingStopLossOrderRejectTransaction represents the rejection of the
     * creation of a TrailingStopLoss Order.
     * <p>
     * @return the trailingStopLossOrderRejectTransaction
     * @see TrailingStopLossOrderRejectTransaction
     */
    public TrailingStopLossOrderRejectTransaction getTrailingStopLossOrderRejectTransaction() {
        return this.trailingStopLossOrderRejectTransaction;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }

    @JsonbProperty("relatedTransactionIDs") private ArrayList<TransactionID> relatedTransactionIDs;

    /**
     * Get the relatedTransactionIDs
     * <p>
     * The IDs of all Transactions that were created while satisfying the
     * request.
     * <p>
     * @return the relatedTransactionIDs
     * @see TransactionID
     */
    public List<TransactionID> getRelatedTransactionIDs() {
        return this.relatedTransactionIDs;
    }
}
