package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.user;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The specifier that refers to a User
 * <p>
 * A user specifier may have one of 3 formats: the OANDA-assigned User ID
 * (numerical), the client-provided username prefixed by the "@" symbol (e.g.
 * "@myusername"), or the "@" symbol. The "@" symbol on its own acts as an
 * alias for the username of the user accessing the endpoint (as inferred from
 * the token provided).
 */
@JsonbTypeAdapter(UserSpecifier.JsonAdapter.class)
public class UserSpecifier extends StringPrimitive {

    /**
     * UserSpecifier copy constructor.
     * <p>
     *
     * @param userSpecifier the
     */
    public UserSpecifier(UserSpecifier userSpecifier) {
        super(userSpecifier.toString());
    }

    /**
     * UserSpecifier constructor.
     * <p>
     *
     * @param userSpecifier the UserSpecifier as a String
     */
    public UserSpecifier(String userSpecifier) {
        super(userSpecifier);
    }

    /**
     * JSON adapter for reading and writing UserSpecifier0
     */
    public static class JsonAdapter implements JsonbAdapter<UserSpecifier, String> {
        @Override
        public String adaptToJson(UserSpecifier userSpecifier) {
            return userSpecifier.toString();
        }

        @Override
        public UserSpecifier adaptFromJson(String s) {
            return new UserSpecifier(s);
        }
    }
}
