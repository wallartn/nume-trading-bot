package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.RequestException;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.ClientConfigureRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * AccountConfigure400RequestException
 */
public class AccountConfigure400RequestException extends RequestException {

    private static final long serialVersionUID = -6799520103822227394L;

    /**
     * AccountConfigure400RequestException Constructor
     * <p>
     * Construct a new AccountConfigure400RequestException
     */
    private AccountConfigure400RequestException() {
    }

    @JsonbProperty("clientConfigureRejectTransaction") private ClientConfigureRejectTransaction clientConfigureRejectTransaction;

    /**
     * Get the clientConfigureRejectTransaction
     * <p>
     * The transaction that rejects the configuration of the Account.
     * <p>
     * @return the clientConfigureRejectTransaction
     * @see ClientConfigureRejectTransaction
     */
    public ClientConfigureRejectTransaction getClientConfigureRejectTransaction() {
        return this.clientConfigureRejectTransaction;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the last Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
