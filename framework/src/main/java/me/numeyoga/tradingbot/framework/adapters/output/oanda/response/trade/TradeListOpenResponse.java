package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * TradeListOpenResponse
 */
public class TradeListOpenResponse {

    /**
     * TradeListOpenResponse Constructor
     * <p>
     * Construct a new TradeListOpenResponse
     */
    private TradeListOpenResponse() {
    }

    @JsonbProperty("trades") private ArrayList<Trade> trades;

    /**
     * Get the trades
     * <p>
     * The Account's list of open Trades
     * <p>
     * @return the trades
     * @see Trade
     */
    public List<Trade> getTrades() {
        return this.trades;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
