package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.RequestException;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TradeClientExtensionsModifyRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * TradeSetClientExtensions400RequestException
 */
public class TradeSetClientExtensions400RequestException extends RequestException {

    private static final long serialVersionUID = -5747287785024911209L;

    /**
     * TradeSetClientExtensions400RequestException Constructor
     * <p>
     * Construct a new TradeSetClientExtensions400RequestException
     */
    private TradeSetClientExtensions400RequestException() {
    }

    @JsonbProperty("tradeClientExtensionsModifyRejectTransaction") private TradeClientExtensionsModifyRejectTransaction tradeClientExtensionsModifyRejectTransaction;

    /**
     * Get the tradeClientExtensionsModifyRejectTransaction
     * <p>
     * The Transaction that rejects the modification of the Trade's Client
     * Extensions.
     * <p>
     * @return the tradeClientExtensionsModifyRejectTransaction
     * @see TradeClientExtensionsModifyRejectTransaction
     */
    public TradeClientExtensionsModifyRejectTransaction getTradeClientExtensionsModifyRejectTransaction() {
        return this.tradeClientExtensionsModifyRejectTransaction;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }

    @JsonbProperty("relatedTransactionIDs") private ArrayList<TransactionID> relatedTransactionIDs;

    /**
     * Get the relatedTransactionIDs
     * <p>
     * The IDs of all Transactions that were created while satisfying the
     * request.
     * <p>
     * @return the relatedTransactionIDs
     * @see TransactionID
     */
    public List<TransactionID> getRelatedTransactionIDs() {
        return this.relatedTransactionIDs;
    }
}
