package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.position;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * PositionGetResponse
 */
public class PositionGetResponse {

    /**
     * PositionGetResponse Constructor
     * <p>
     * Construct a new PositionGetResponse
     */
    private PositionGetResponse() {
    }

    @JsonbProperty("position") private Position position;

    /**
     * Get the position
     * <p>
     * The requested Position.
     * <p>
     * @return the position
     * @see Position
     */
    public Position getPosition() {
        return this.position;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
