package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives;

/**
 * The type of an Instrument.
 */
public enum InstrumentType {

    /**
     * Currency
     */
    CURRENCY,

    /**
     * Contract For Difference
     */
    CFD,

    /**
     * Metal
     */
    METAL
}
