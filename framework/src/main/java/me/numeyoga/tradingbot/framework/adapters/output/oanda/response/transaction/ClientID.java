package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * A client-provided identifier, used by clients to refer to their Orders or
 * Trades with an identifier that they have provided.
 */
@JsonbTypeAdapter(ClientID.JsonAdapter.class)
public class ClientID extends StringPrimitive {

    /**
     * ClientID copy constructor.
     * <p>
     * @param clientID the 
     */
    public ClientID(ClientID clientID)
    {
        super(clientID.toString());
    }

    /**
     * ClientID constructor.
     * <p>
     * @param clientID the ClientID as a String
     */
    public ClientID(String clientID) {
        super(clientID);
    }

    /**
     * JSON adapter for reading and writing ClientID0
     */
    public static class JsonAdapter implements JsonbAdapter<ClientID, String> {

        @Override
        public String adaptToJson(ClientID clientID) {
            return clientID.toString();
        }

        @Override
        public ClientID adaptFromJson(String s) {
            return new ClientID(s);
        }
    }
}
