package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.user;

import lombok.NoArgsConstructor;

import javax.json.bind.annotation.JsonbProperty;

/**
 * A representation of user information, as available to external (3rd party)
 * clients.
 */
@NoArgsConstructor
public class UserInfoExternal {

    @JsonbProperty("userID")
    private Long userID;
    @JsonbProperty("country")
    private String country;
    @JsonbProperty("FIFO")
    private Boolean fIFO;

    /**
     * Copy constructor
     * <p>
     *
     * @param other the UserInfoExternal to copy
     */
    public UserInfoExternal(UserInfoExternal other) {
        if (other.userID != null) {
            this.userID = other.userID;
        }
        this.country = other.country;
        if (other.fIFO != null) {
            this.fIFO = other.fIFO;
        }
    }

    /**
     * Get the userID
     * <p>
     * The user's OANDA-assigned user ID.
     * <p>
     *
     * @return the userID
     */
    public Long getUserID() {
        return this.userID;
    }

    /**
     * Set the userID
     * <p>
     * The user's OANDA-assigned user ID.
     * <p>
     *
     * @param userID the userID as a Long
     * @return {@link UserInfoExternal UserInfoExternal}
     */
    public UserInfoExternal setUserID(Long userID) {
        this.userID = userID;
        return this;
    }

    /**
     * Get the country
     * <p>
     * The country that the user is based in.
     * <p>
     *
     * @return the country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * Set the country
     * <p>
     * The country that the user is based in.
     * <p>
     *
     * @param country the country as a String
     * @return {@link UserInfoExternal UserInfoExternal}
     */
    public UserInfoExternal setCountry(String country) {
        this.country = country;
        return this;
    }

    /**
     * Get the FIFO
     * <p>
     * Flag indicating if the user's Accounts adhere to FIFO execution
     * rules.
     * <p>
     *
     * @return the FIFO
     */
    public Boolean getFIFO() {
        return this.fIFO;
    }

    /**
     * Set the FIFO
     * <p>
     * Flag indicating if the user's Accounts adhere to FIFO execution
     * rules.
     * <p>
     *
     * @param fIFO the FIFO as a Boolean
     * @return {@link UserInfoExternal UserInfoExternal}
     */
    public UserInfoExternal setFIFO(Boolean fIFO) {
        this.fIFO = fIFO;
        return this;
    }

    @Override
    public String toString() {
        return "UserInfoExternal(" + "userID=" + (userID == null ? "null" : userID.toString()) + ", " + "country=" + (country == null ? "null" : country) + ", " + "FIFO=" + (fIFO == null ? "null" : fIFO.toString()) + ")";
    }
}
