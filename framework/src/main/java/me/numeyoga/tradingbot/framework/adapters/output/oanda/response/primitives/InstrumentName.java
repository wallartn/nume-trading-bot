package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * Instrument name identifier. Used by clients to refer to an Instrument.
 * <p>
 * A string containing the base currency and quote currency delimited by a "_".
 */
@JsonbTypeAdapter(InstrumentName.JsonAdapter.class)
public class InstrumentName extends StringPrimitive {

    /**
     * InstrumentName copy constructor.
     * <p>
     * @param instrumentName the 
     */
    public InstrumentName(InstrumentName instrumentName)
    {
        super(instrumentName.toString());
    }

    /**
     * InstrumentName constructor.
     * <p>
     * @param instrumentName the InstrumentName as a String
     */
    public InstrumentName(String instrumentName) {
        super(instrumentName);
    }

    /**
     * JSON adapter for reading and writing InstrumentName0
     */
    public static class JsonAdapter implements JsonbAdapter<InstrumentName, String> {

        @Override
        public String adaptToJson(InstrumentName instrumentName) {
            return instrumentName.toString();
        }

        @Override
        public InstrumentName adaptFromJson(String s){
            return new InstrumentName(s);
        }
    }
}
