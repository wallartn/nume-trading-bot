package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import lombok.NoArgsConstructor;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order.OrderID;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common.PriceValue;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.AccountUnits;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.DateTime;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.DecimalNumber;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.InstrumentName;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.ClientExtensions;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.annotation.JsonbProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The summary of a Trade within an Account. This representation does not
 * provide the full details of the Trade's dependent Orders.
 * <p>
 * {currentUnits} ({initialUnits}) of {instrument} @ {price}
 */
@NoArgsConstructor
public class TradeSummary {

    @JsonbProperty("id")
    TradeID id;
    @JsonbProperty("instrument")
    InstrumentName instrument;
    @JsonbProperty("price")
    PriceValue price;
    @JsonbProperty("openTime")
    DateTime openTime;
    @JsonbProperty("state")
    TradeState state;
    @JsonbProperty("initialUnits")
    DecimalNumber initialUnits;
    @JsonbProperty("initialMarginRequired")
    AccountUnits initialMarginRequired;
    @JsonbProperty("currentUnits")
    DecimalNumber currentUnits;
    @JsonbProperty("realizedPL")
    AccountUnits realizedPL;
    @JsonbProperty("unrealizedPL")
    AccountUnits unrealizedPL;
    @JsonbProperty("marginUsed")
    AccountUnits marginUsed;
    @JsonbProperty("averageClosePrice")
    PriceValue averageClosePrice;
    @JsonbProperty("closingTransactionIDs")
    List<TransactionID> closingTransactionIDs;
    @JsonbProperty("financing")
    AccountUnits financing;
    @JsonbProperty("closeTime")
    DateTime closeTime;
    @JsonbProperty("clientExtensions")
    ClientExtensions clientExtensions;
    @JsonbProperty("takeProfitOrderID")
    OrderID takeProfitOrderID;
    @JsonbProperty("stopLossOrderID")
    OrderID stopLossOrderID;
    @JsonbProperty("trailingStopLossOrderID")
    OrderID trailingStopLossOrderID;

    /**
     * Copy constructor
     * <p>
     *
     * @param other the TradeSummary to copy
     */
    public TradeSummary(TradeSummary other) {
        this.id = other.id;
        this.instrument = other.instrument;
        this.price = other.price;
        this.openTime = other.openTime;
        this.state = other.state;
        this.initialUnits = other.initialUnits;
        this.initialMarginRequired = other.initialMarginRequired;
        this.currentUnits = other.currentUnits;
        this.realizedPL = other.realizedPL;
        this.unrealizedPL = other.unrealizedPL;
        this.marginUsed = other.marginUsed;
        this.averageClosePrice = other.averageClosePrice;
        if (other.closingTransactionIDs != null) {
            this.closingTransactionIDs = new ArrayList<>(other.closingTransactionIDs);
        }
        this.financing = other.financing;
        this.closeTime = other.closeTime;
        if (other.clientExtensions != null) {
            this.clientExtensions = new ClientExtensions(other.clientExtensions);
        }
        this.takeProfitOrderID = other.takeProfitOrderID;
        this.stopLossOrderID = other.stopLossOrderID;
        this.trailingStopLossOrderID = other.trailingStopLossOrderID;
    }

    /**
     * Get the Trade ID
     * <p>
     * The Trade's identifier, unique within the Trade's Account.
     * <p>
     *
     * @return the Trade ID
     * @see TradeID
     */
    public TradeID getId() {
        return this.id;
    }

    /**
     * Set the Trade ID
     * <p>
     * The Trade's identifier, unique within the Trade's Account.
     * <p>
     *
     * @param id the Trade ID as a TradeID
     * @return {@link TradeSummary TradeSummary}
     * @see TradeID
     */
    public TradeSummary setId(TradeID id) {
        this.id = id;
        return this;
    }

    /**
     * Get the Instrument
     * <p>
     * The Trade's Instrument.
     * <p>
     *
     * @return the Instrument
     * @see InstrumentName
     */
    public InstrumentName getInstrument() {
        return this.instrument;
    }

    /**
     * Set the Instrument
     * <p>
     * The Trade's Instrument.
     * <p>
     *
     * @param instrument the Instrument as an InstrumentName
     * @return {@link TradeSummary TradeSummary}
     * @see InstrumentName
     */
    public TradeSummary setInstrument(InstrumentName instrument) {
        this.instrument = instrument;
        return this;
    }

    /**
     * Get the Fill Price
     * <p>
     * The execution price of the Trade.
     * <p>
     *
     * @return the Fill Price
     * @see PriceValue
     */
    public PriceValue getPrice() {
        return this.price;
    }

    /**
     * Set the Fill Price
     * <p>
     * The execution price of the Trade.
     * <p>
     *
     * @param price the Fill Price as a PriceValue
     * @return {@link TradeSummary TradeSummary}
     * @see PriceValue
     */
    public TradeSummary setPrice(PriceValue price) {
        this.price = price;
        return this;
    }

    /**
     * Get the Open Time
     * <p>
     * The date/time when the Trade was opened.
     * <p>
     *
     * @return the Open Time
     * @see DateTime
     */
    public DateTime getOpenTime() {
        return this.openTime;
    }

    /**
     * Set the Open Time
     * <p>
     * The date/time when the Trade was opened.
     * <p>
     *
     * @param openTime the Open Time as a DateTime
     * @return {@link TradeSummary TradeSummary}
     * @see DateTime
     */
    public TradeSummary setOpenTime(DateTime openTime) {
        this.openTime = openTime;
        return this;
    }

    /**
     * Get the State
     * <p>
     * The current state of the Trade.
     * <p>
     *
     * @return the State
     * @see TradeState
     */
    public TradeState getState() {
        return this.state;
    }

    /**
     * Set the State
     * <p>
     * The current state of the Trade.
     * <p>
     *
     * @param state the State as a TradeState
     * @return {@link TradeSummary TradeSummary}
     * @see TradeState
     */
    public TradeSummary setState(TradeState state) {
        this.state = state;
        return this;
    }

    /**
     * Get the Initial Trade Units
     * <p>
     * The initial size of the Trade. Negative values indicate a short Trade,
     * and positive values indicate a long Trade.
     * <p>
     *
     * @return the Initial Trade Units
     * @see DecimalNumber
     */
    public DecimalNumber getInitialUnits() {
        return this.initialUnits;
    }

    /**
     * Set the Initial Trade Units
     * <p>
     * The initial size of the Trade. Negative values indicate a short Trade,
     * and positive values indicate a long Trade.
     * <p>
     *
     * @param initialUnits the Initial Trade Units as a DecimalNumber
     * @return {@link TradeSummary TradeSummary}
     * @see DecimalNumber
     */
    public TradeSummary setInitialUnits(DecimalNumber initialUnits) {
        this.initialUnits = initialUnits;
        return this;
    }

    /**
     * Get the Initial Margin Required
     * <p>
     * The margin required at the time the Trade was created. Note, this is the
     * 'pure' margin required, it is not the 'effective' margin used that
     * factors in the trade risk if a GSLO is attached to the trade.
     * <p>
     *
     * @return the Initial Margin Required
     * @see AccountUnits
     */
    public AccountUnits getInitialMarginRequired() {
        return this.initialMarginRequired;
    }

    /**
     * Set the Initial Margin Required
     * <p>
     * The margin required at the time the Trade was created. Note, this is the
     * 'pure' margin required, it is not the 'effective' margin used that
     * factors in the trade risk if a GSLO is attached to the trade.
     * <p>
     *
     * @param initialMarginRequired the Initial Margin Required as an
     *                              AccountUnits
     * @return {@link TradeSummary TradeSummary}
     * @see AccountUnits
     */
    public TradeSummary setInitialMarginRequired(AccountUnits initialMarginRequired) {
        this.initialMarginRequired = initialMarginRequired;
        return this;
    }

    /**
     * Get the Current Open Trade Units
     * <p>
     * The number of units currently open for the Trade. This value is reduced
     * to 0.0 as the Trade is closed.
     * <p>
     *
     * @return the Current Open Trade Units
     * @see DecimalNumber
     */
    public DecimalNumber getCurrentUnits() {
        return this.currentUnits;
    }

    /**
     * Set the Current Open Trade Units
     * <p>
     * The number of units currently open for the Trade. This value is reduced
     * to 0.0 as the Trade is closed.
     * <p>
     *
     * @param currentUnits the Current Open Trade Units as a DecimalNumber
     * @return {@link TradeSummary TradeSummary}
     * @see DecimalNumber
     */
    public TradeSummary setCurrentUnits(DecimalNumber currentUnits) {
        this.currentUnits = currentUnits;
        return this;
    }

    /**
     * Get the Realized Profit/Loss
     * <p>
     * The total profit/loss realized on the closed portion of the Trade.
     * <p>
     *
     * @return the Realized Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getRealizedPL() {
        return this.realizedPL;
    }

    /**
     * Set the Realized Profit/Loss
     * <p>
     * The total profit/loss realized on the closed portion of the Trade.
     * <p>
     *
     * @param realizedPL the Realized Profit/Loss as an AccountUnits
     * @return {@link TradeSummary TradeSummary}
     * @see AccountUnits
     */
    public TradeSummary setRealizedPL(AccountUnits realizedPL) {
        this.realizedPL = realizedPL;
        return this;
    }

    /**
     * Get the Unrealized Profit/Loss
     * <p>
     * The unrealized profit/loss on the open portion of the Trade.
     * <p>
     *
     * @return the Unrealized Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getUnrealizedPL() {
        return this.unrealizedPL;
    }

    /**
     * Set the Unrealized Profit/Loss
     * <p>
     * The unrealized profit/loss on the open portion of the Trade.
     * <p>
     *
     * @param unrealizedPL the Unrealized Profit/Loss as an AccountUnits
     * @return {@link TradeSummary TradeSummary}
     * @see AccountUnits
     */
    public TradeSummary setUnrealizedPL(AccountUnits unrealizedPL) {
        this.unrealizedPL = unrealizedPL;
        return this;
    }

    /**
     * Get the Margin Used
     * <p>
     * Margin currently used by the Trade.
     * <p>
     *
     * @return the Margin Used
     * @see AccountUnits
     */
    public AccountUnits getMarginUsed() {
        return this.marginUsed;
    }

    /**
     * Set the Margin Used
     * <p>
     * Margin currently used by the Trade.
     * <p>
     *
     * @param marginUsed the Margin Used as an AccountUnits
     * @return {@link TradeSummary TradeSummary}
     * @see AccountUnits
     */
    public TradeSummary setMarginUsed(AccountUnits marginUsed) {
        this.marginUsed = marginUsed;
        return this;
    }

    /**
     * Get the Average Close Price
     * <p>
     * The average closing price of the Trade. Only present if the Trade has
     * been closed or reduced at least once.
     * <p>
     *
     * @return the Average Close Price
     * @see PriceValue
     */
    public PriceValue getAverageClosePrice() {
        return this.averageClosePrice;
    }

    /**
     * Set the Average Close Price
     * <p>
     * The average closing price of the Trade. Only present if the Trade has
     * been closed or reduced at least once.
     * <p>
     *
     * @param averageClosePrice the Average Close Price as a PriceValue
     * @return {@link TradeSummary TradeSummary}
     * @see PriceValue
     */
    public TradeSummary setAverageClosePrice(PriceValue averageClosePrice) {
        this.averageClosePrice = averageClosePrice;
        return this;
    }

    /**
     * Get the Closing Transaction IDs
     * <p>
     * The IDs of the Transactions that have closed portions of this Trade.
     * <p>
     *
     * @return the Closing Transaction IDs
     * @see TransactionID
     */
    public List<TransactionID> getClosingTransactionIDs() {
        return this.closingTransactionIDs;
    }

    /**
     * Set the Closing Transaction IDs
     * <p>
     * The IDs of the Transactions that have closed portions of this Trade.
     * <p>
     *
     * @param closingTransactionIDs the Closing Transaction IDs
     * @return {@link TradeSummary TradeSummary}
     * @see TransactionID
     */
    public TradeSummary setClosingTransactionIDs(List<TransactionID> closingTransactionIDs) {
        this.closingTransactionIDs = closingTransactionIDs;
        return this;
    }

    /**
     * Get the Financing
     * <p>
     * The financing paid/collected for this Trade.
     * <p>
     *
     * @return the Financing
     * @see AccountUnits
     */
    public AccountUnits getFinancing() {
        return this.financing;
    }

    /**
     * Set the Financing
     * <p>
     * The financing paid/collected for this Trade.
     * <p>
     *
     * @param financing the Financing as an AccountUnits
     * @return {@link TradeSummary TradeSummary}
     * @see AccountUnits
     */
    public TradeSummary setFinancing(AccountUnits financing) {
        this.financing = financing;
        return this;
    }

    /**
     * Get the Close Time
     * <p>
     * The date/time when the Trade was fully closed. Only provided for Trades
     * whose state is CLOSED.
     * <p>
     *
     * @return the Close Time
     * @see DateTime
     */
    public DateTime getCloseTime() {
        return this.closeTime;
    }

    /**
     * Set the Close Time
     * <p>
     * The date/time when the Trade was fully closed. Only provided for Trades
     * whose state is CLOSED.
     * <p>
     *
     * @param closeTime the Close Time as a DateTime
     * @return {@link TradeSummary TradeSummary}
     * @see DateTime
     */
    public TradeSummary setCloseTime(DateTime closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    /**
     * Get the Client Extensions
     * <p>
     * The client extensions of the Trade.
     * <p>
     *
     * @return the Client Extensions
     * @see ClientExtensions
     */
    public ClientExtensions getClientExtensions() {
        return this.clientExtensions;
    }

    /**
     * Set the Client Extensions
     * <p>
     * The client extensions of the Trade.
     * <p>
     *
     * @param clientExtensions the Client Extensions as a ClientExtensions
     * @return {@link TradeSummary TradeSummary}
     * @see ClientExtensions
     */
    public TradeSummary setClientExtensions(ClientExtensions clientExtensions) {
        this.clientExtensions = clientExtensions;
        return this;
    }

    /**
     * Get the Take Profit Order ID
     * <p>
     * ID of the Trade's Take Profit Order, only provided if such an Order
     * exists.
     * <p>
     *
     * @return the Take Profit Order ID
     * @see OrderID
     */
    public OrderID getTakeProfitOrderID() {
        return this.takeProfitOrderID;
    }

    /**
     * Set the Take Profit Order ID
     * <p>
     * ID of the Trade's Take Profit Order, only provided if such an Order
     * exists.
     * <p>
     *
     * @param takeProfitOrderID the Take Profit Order ID as an OrderID
     * @return {@link TradeSummary TradeSummary}
     * @see OrderID
     */
    public TradeSummary setTakeProfitOrderID(OrderID takeProfitOrderID) {
        this.takeProfitOrderID = takeProfitOrderID;
        return this;
    }

    /**
     * Get the Stop Loss Order ID
     * <p>
     * ID of the Trade's Stop Loss Order, only provided if such an Order
     * exists.
     * <p>
     *
     * @return the Stop Loss Order ID
     * @see OrderID
     */
    public OrderID getStopLossOrderID() {
        return this.stopLossOrderID;
    }

    /**
     * Set the Stop Loss Order ID
     * <p>
     * ID of the Trade's Stop Loss Order, only provided if such an Order
     * exists.
     * <p>
     *
     * @param stopLossOrderID the Stop Loss Order ID as an OrderID
     * @return {@link TradeSummary TradeSummary}
     * @see OrderID
     */
    public TradeSummary setStopLossOrderID(OrderID stopLossOrderID) {
        this.stopLossOrderID = stopLossOrderID;
        return this;
    }

    /**
     * Get the Trailing Stop Loss Order ID
     * <p>
     * ID of the Trade's Trailing Stop Loss Order, only provided if such an
     * Order exists.
     * <p>
     *
     * @return the Trailing Stop Loss Order ID
     * @see OrderID
     */
    public OrderID getTrailingStopLossOrderID() {
        return this.trailingStopLossOrderID;
    }

    /**
     * Set the Trailing Stop Loss Order ID
     * <p>
     * ID of the Trade's Trailing Stop Loss Order, only provided if such an
     * Order exists.
     * <p>
     *
     * @param trailingStopLossOrderID the Trailing Stop Loss Order ID as an
     *                                OrderID
     * @return {@link TradeSummary TradeSummary}
     * @see OrderID
     */
    public TradeSummary setTrailingStopLossOrderID(OrderID trailingStopLossOrderID) {
        this.trailingStopLossOrderID = trailingStopLossOrderID;
        return this;
    }

    @Override
    public String toString() {
        return "TradeSummary(" +
                "id=" +
                (id == null ? "null" : id.toString()) + ", " +
                "instrument=" +
                (instrument == null ? "null" : instrument.toString()) + ", " +
                "price=" +
                (price == null ? "null" : price.toString()) + ", " +
                "openTime=" +
                (openTime == null ? "null" : openTime.toString()) + ", " +
                "state=" +
                (state == null ? "null" : state.toString()) + ", " +
                "initialUnits=" +
                (initialUnits == null ? "null" : initialUnits.toString()) + ", " +
                "initialMarginRequired=" +
                (initialMarginRequired == null ? "null" : initialMarginRequired.toString()) + ", " +
                "currentUnits=" +
                (currentUnits == null ? "null" : currentUnits.toString()) + ", " +
                "realizedPL=" +
                (realizedPL == null ? "null" : realizedPL.toString()) + ", " +
                "unrealizedPL=" +
                (unrealizedPL == null ? "null" : unrealizedPL.toString()) + ", " +
                "marginUsed=" +
                (marginUsed == null ? "null" : marginUsed.toString()) + ", " +
                "averageClosePrice=" +
                (averageClosePrice == null ? "null" : averageClosePrice.toString()) + ", " +
                "closingTransactionIDs=" +
                (closingTransactionIDs == null ? "null" : closingTransactionIDs.toString()) + ", " +
                "financing=" +
                (financing == null ? "null" : financing.toString()) + ", " +
                "closeTime=" +
                (closeTime == null ? "null" : closeTime.toString()) + ", " +
                "clientExtensions=" +
                (clientExtensions == null ? "null" : clientExtensions.toString()) + ", " +
                "takeProfitOrderID=" +
                (takeProfitOrderID == null ? "null" : takeProfitOrderID.toString()) + ", " +
                "stopLossOrderID=" +
                (stopLossOrderID == null ? "null" : stopLossOrderID.toString()) + ", " +
                "trailingStopLossOrderID=" +
                (trailingStopLossOrderID == null ? "null" : trailingStopLossOrderID.toString()) +
                ")";
    }
}
