package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.user;

import javax.json.bind.annotation.JsonbProperty;

/**
 * UserGetInfoResponse
 */
public class UserGetInfoResponse {

    @JsonbProperty("userInfo")
    private UserInfo userInfo;

    /**
     * UserGetInfoResponse Constructor
     * <p>
     * Construct a new UserGetInfoResponse
     */
    private UserGetInfoResponse() {
    }

    /**
     * Get the userInfo
     * <p>
     * The user information for the specifier user.
     * <p>
     *
     * @return the userInfo
     * @see UserInfo
     */
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
}
