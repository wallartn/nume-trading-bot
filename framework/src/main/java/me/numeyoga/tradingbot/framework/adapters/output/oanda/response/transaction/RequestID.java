package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The request identifier.
 */
@JsonbTypeAdapter(RequestID.JsonAdapter.class)
public class RequestID extends StringPrimitive {

    /**
     * RequestID copy constructor.
     * <p>
     * @param requestID the 
     */
    public RequestID(RequestID requestID)
    {
        super(requestID.toString());
    }

    /**
     * RequestID constructor.
     * <p>
     * @param requestID the RequestID as a String
     */
    public RequestID(String requestID) {
        super(requestID);
    }

    /**
     * JSON adapter for reading and writing RequestID0
     */
    public static class JsonAdapter implements JsonbAdapter<RequestID, String> {

        @Override
        public String adaptToJson(RequestID requestID) {
            return requestID.toString();
        }

        @Override
        public RequestID adaptFromJson(String s) {
            return new RequestID(s);
        }
    }
}
