package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

/**
 * The reason that the Fixed Price Order was created
 */
public enum FixedPriceOrderReason {

    /**
     * The Fixed Price Order was created as part of a platform account
     * migration
     */
    PLATFORM_ACCOUNT_MIGRATION
}
