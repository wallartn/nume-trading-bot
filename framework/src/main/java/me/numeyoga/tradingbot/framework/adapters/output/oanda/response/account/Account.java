package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import lombok.NoArgsConstructor;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order.Order;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.position.Position;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.AccountUnits;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.Currency;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.DateTime;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.DecimalNumber;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade.TradeSummary;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.annotation.JsonbProperty;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The full details of a client's Account. This includes full open Trade, open
 * Position and pending Order representation.
 * <p>
 * Account {id}
 */
@NoArgsConstructor
public class Account {

    @JsonbProperty("id")
    AccountID id;
    @JsonbProperty("alias")
    String alias;
    @JsonbProperty("currency")
    Currency currency;
    @JsonbProperty("balance")
    AccountUnits balance;
    @JsonbProperty("createdByUserID")
    Long createdByUserID;
    @JsonbProperty("createdTime")
    DateTime createdTime;
    @JsonbProperty("guaranteedStopLossOrderMode")
    GuaranteedStopLossOrderMode guaranteedStopLossOrderMode;
    @JsonbProperty("pl")
    AccountUnits pl;
    @JsonbProperty("resettablePL")
    AccountUnits resettablePL;
    @JsonbProperty("resettablePLTime")
    DateTime resettablePLTime;
    @JsonbProperty("financing")
    AccountUnits financing;
    @JsonbProperty("commission")
    AccountUnits commission;
    @JsonbProperty("guaranteedExecutionFees")
    AccountUnits guaranteedExecutionFees;
    @JsonbProperty("marginRate")
    DecimalNumber marginRate;
    @JsonbProperty("marginCallEnterTime")
    DateTime marginCallEnterTime;
    @JsonbProperty("marginCallExtensionCount")
    Long marginCallExtensionCount;
    @JsonbProperty("lastMarginCallExtensionTime")
    DateTime lastMarginCallExtensionTime;
    @JsonbProperty("openTradeCount")
    Long openTradeCount;
    @JsonbProperty("openPositionCount")
    Long openPositionCount;
    @JsonbProperty("pendingOrderCount")
    Long pendingOrderCount;
    @JsonbProperty("hedgingEnabled")
    Boolean hedgingEnabled;
    @JsonbProperty("lastOrderFillTimestamp")
    DateTime lastOrderFillTimestamp;
    @JsonbProperty("unrealizedPL")
    AccountUnits unrealizedPL;
    @JsonbProperty("NAV")
    AccountUnits nAV;
    @JsonbProperty("marginUsed")
    AccountUnits marginUsed;
    @JsonbProperty("marginAvailable")
    AccountUnits marginAvailable;
    @JsonbProperty("positionValue")
    AccountUnits positionValue;
    @JsonbProperty("marginCloseoutUnrealizedPL")
    AccountUnits marginCloseoutUnrealizedPL;
    @JsonbProperty("marginCloseoutNAV")
    AccountUnits marginCloseoutNAV;
    @JsonbProperty("marginCloseoutMarginUsed")
    AccountUnits marginCloseoutMarginUsed;
    @JsonbProperty("marginCloseoutPercent")
    DecimalNumber marginCloseoutPercent;
    @JsonbProperty("marginCloseoutPositionValue")
    DecimalNumber marginCloseoutPositionValue;
    @JsonbProperty("withdrawalLimit")
    AccountUnits withdrawalLimit;
    @JsonbProperty("marginCallMarginUsed")
    AccountUnits marginCallMarginUsed;
    @JsonbProperty("marginCallPercent")
    DecimalNumber marginCallPercent;
    @JsonbProperty("lastTransactionID")
    TransactionID lastTransactionID;
    @JsonbProperty("trades")
    List<TradeSummary> trades;
    @JsonbProperty("positions")
    List<Position> positions;
    @JsonbProperty("orders")
    List<Order> orders;

    /**
     * Copy constructor
     * <p>
     *
     * @param other the Account to copy
     */
    public Account(Account other) {
        this.id = other.id;
        this.alias = other.alias;
        this.currency = other.currency;
        this.balance = other.balance;
        if (other.createdByUserID != null) {
            this.createdByUserID = other.createdByUserID;
        }
        this.createdTime = other.createdTime;
        this.guaranteedStopLossOrderMode = other.guaranteedStopLossOrderMode;
        this.pl = other.pl;
        this.resettablePL = other.resettablePL;
        this.resettablePLTime = other.resettablePLTime;
        this.financing = other.financing;
        this.commission = other.commission;
        this.guaranteedExecutionFees = other.guaranteedExecutionFees;
        this.marginRate = other.marginRate;
        this.marginCallEnterTime = other.marginCallEnterTime;
        if (other.marginCallExtensionCount != null) {
            this.marginCallExtensionCount = other.marginCallExtensionCount;
        }
        this.lastMarginCallExtensionTime = other.lastMarginCallExtensionTime;
        if (other.openTradeCount != null) {
            this.openTradeCount = other.openTradeCount;
        }
        if (other.openPositionCount != null) {
            this.openPositionCount = other.openPositionCount;
        }
        if (other.pendingOrderCount != null) {
            this.pendingOrderCount = other.pendingOrderCount;
        }
        if (other.hedgingEnabled != null) {
            this.hedgingEnabled = other.hedgingEnabled;
        }
        this.lastOrderFillTimestamp = other.lastOrderFillTimestamp;
        this.unrealizedPL = other.unrealizedPL;
        this.nAV = other.nAV;
        this.marginUsed = other.marginUsed;
        this.marginAvailable = other.marginAvailable;
        this.positionValue = other.positionValue;
        this.marginCloseoutUnrealizedPL = other.marginCloseoutUnrealizedPL;
        this.marginCloseoutNAV = other.marginCloseoutNAV;
        this.marginCloseoutMarginUsed = other.marginCloseoutMarginUsed;
        this.marginCloseoutPercent = other.marginCloseoutPercent;
        this.marginCloseoutPositionValue = other.marginCloseoutPositionValue;
        this.withdrawalLimit = other.withdrawalLimit;
        this.marginCallMarginUsed = other.marginCallMarginUsed;
        this.marginCallPercent = other.marginCallPercent;
        this.lastTransactionID = other.lastTransactionID;
        if (other.trades != null) {
            this.trades = new ArrayList<>(other.trades);
        }
        if (other.positions != null) {
            this.positions = new ArrayList<>(other.positions);
        }
        if (other.orders != null) {
            this.orders = new ArrayList<>(other.orders);
        }
    }

    /**
     * Get the Account ID
     * <p>
     * The Account's identifier
     * <p>
     *
     * @return the Account ID
     * @see AccountID
     */
    public AccountID getId() {
        return this.id;
    }

    /**
     * Set the Account ID
     * <p>
     * The Account's identifier
     * <p>
     *
     * @param id the Account ID as an AccountID
     * @return {@link Account Account}
     * @see AccountID
     */
    public Account setId(AccountID id) {
        this.id = id;
        return this;
    }

    /**
     * Get the Alias
     * <p>
     * Client-assigned alias for the Account. Only provided if the Account has
     * an alias set
     * <p>
     *
     * @return the Alias
     */
    public String getAlias() {
        return this.alias;
    }

    /**
     * Set the Alias
     * <p>
     * Client-assigned alias for the Account. Only provided if the Account has
     * an alias set
     * <p>
     *
     * @param alias the Alias as a String
     * @return {@link Account Account}
     */
    public Account setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    /**
     * Get the Home Currency
     * <p>
     * The home currency of the Account
     * <p>
     *
     * @return the Home Currency
     * @see Currency
     */
    public Currency getCurrency() {
        return this.currency;
    }

    /**
     * Set the Home Currency
     * <p>
     * The home currency of the Account
     * <p>
     *
     * @param currency the Home Currency as a Currency
     * @return {@link Account Account}
     * @see Currency
     */
    public Account setCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Get the Balance
     * <p>
     * The current balance of the Account.
     * <p>
     *
     * @return the Balance
     * @see AccountUnits
     */
    public AccountUnits getBalance() {
        return this.balance;
    }

    /**
     * Set the Balance
     * <p>
     * The current balance of the Account.
     * <p>
     *
     * @param balance the Balance as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setBalance(AccountUnits balance) {
        this.balance = balance;
        return this;
    }

    /**
     * Get the Created by User ID
     * <p>
     * ID of the user that created the Account.
     * <p>
     *
     * @return the Created by User ID
     */
    public Long getCreatedByUserID() {
        return this.createdByUserID;
    }

    /**
     * Set the Created by User ID
     * <p>
     * ID of the user that created the Account.
     * <p>
     *
     * @param createdByUserID the Created by User ID as a Long
     * @return {@link Account Account}
     */
    public Account setCreatedByUserID(Long createdByUserID) {
        this.createdByUserID = createdByUserID;
        return this;
    }

    /**
     * Get the Create Time
     * <p>
     * The date/time when the Account was created.
     * <p>
     *
     * @return the Create Time
     * @see DateTime
     */
    public DateTime getCreatedTime() {
        return this.createdTime;
    }

    /**
     * Set the Create Time
     * <p>
     * The date/time when the Account was created.
     * <p>
     *
     * @param createdTime the Create Time as a DateTime
     * @return {@link Account Account}
     * @see DateTime
     */
    public Account setCreatedTime(DateTime createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    /**
     * Get the Guaranteed Stop Loss Order Mode
     * <p>
     * The current guaranteed Stop Loss Order mode of the Account.
     * <p>
     *
     * @return the Guaranteed Stop Loss Order Mode
     * @see GuaranteedStopLossOrderMode
     */
    public GuaranteedStopLossOrderMode getGuaranteedStopLossOrderMode() {
        return this.guaranteedStopLossOrderMode;
    }

    /**
     * Set the Guaranteed Stop Loss Order Mode
     * <p>
     * The current guaranteed Stop Loss Order mode of the Account.
     * <p>
     *
     * @param guaranteedStopLossOrderMode the Guaranteed Stop Loss Order Mode
     *                                    as a GuaranteedStopLossOrderMode
     * @return {@link Account Account}
     * @see GuaranteedStopLossOrderMode
     */
    public Account setGuaranteedStopLossOrderMode(GuaranteedStopLossOrderMode guaranteedStopLossOrderMode) {
        this.guaranteedStopLossOrderMode = guaranteedStopLossOrderMode;
        return this;
    }

    /**
     * Get the Profit/Loss
     * <p>
     * The total profit/loss realized over the lifetime of the Account.
     * <p>
     *
     * @return the Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getPl() {
        return this.pl;
    }

    /**
     * Set the Profit/Loss
     * <p>
     * The total profit/loss realized over the lifetime of the Account.
     * <p>
     *
     * @param pl the Profit/Loss as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setPl(AccountUnits pl) {
        this.pl = pl;
        return this;
    }

    /**
     * Get the Resettable Profit/Loss
     * <p>
     * The total realized profit/loss for the Account since it was last reset
     * by the client.
     * <p>
     *
     * @return the Resettable Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getResettablePL() {
        return this.resettablePL;
    }

    /**
     * Set the Resettable Profit/Loss
     * <p>
     * The total realized profit/loss for the Account since it was last reset
     * by the client.
     * <p>
     *
     * @param resettablePL the Resettable Profit/Loss as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setResettablePL(AccountUnits resettablePL) {
        this.resettablePL = resettablePL;
        return this;
    }

    /**
     * Get the Profit/Loss Reset Time
     * <p>
     * The date/time that the Account's resettablePL was last reset.
     * <p>
     *
     * @return the Profit/Loss Reset Time
     * @see DateTime
     */
    public DateTime getResettablePLTime() {
        return this.resettablePLTime;
    }

    /**
     * Set the Profit/Loss Reset Time
     * <p>
     * The date/time that the Account's resettablePL was last reset.
     * <p>
     *
     * @param resettablePLTime the Profit/Loss Reset Time as a DateTime
     * @return {@link Account Account}
     * @see DateTime
     */
    public Account setResettablePLTime(DateTime resettablePLTime) {
        this.resettablePLTime = resettablePLTime;
        return this;
    }

    /**
     * Get the Financing
     * <p>
     * The total amount of financing paid/collected over the lifetime of the
     * Account.
     * <p>
     *
     * @return the Financing
     * @see AccountUnits
     */
    public AccountUnits getFinancing() {
        return this.financing;
    }

    /**
     * Set the Financing
     * <p>
     * The total amount of financing paid/collected over the lifetime of the
     * Account.
     * <p>
     *
     * @param financing the Financing as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setFinancing(AccountUnits financing) {
        this.financing = financing;
        return this;
    }

    /**
     * Get the Commission
     * <p>
     * The total amount of commission paid over the lifetime of the Account.
     * <p>
     *
     * @return the Commission
     * @see AccountUnits
     */
    public AccountUnits getCommission() {
        return this.commission;
    }

    /**
     * Set the Commission
     * <p>
     * The total amount of commission paid over the lifetime of the Account.
     * <p>
     *
     * @param commission the Commission as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setCommission(AccountUnits commission) {
        this.commission = commission;
        return this;
    }

    /**
     * Get the Guaranteed Execution Fees
     * <p>
     * The total amount of fees charged over the lifetime of the Account for
     * the execution of guaranteed Stop Loss Orders.
     * <p>
     *
     * @return the Guaranteed Execution Fees
     * @see AccountUnits
     */
    public AccountUnits getGuaranteedExecutionFees() {
        return this.guaranteedExecutionFees;
    }

    /**
     * Set the Guaranteed Execution Fees
     * <p>
     * The total amount of fees charged over the lifetime of the Account for
     * the execution of guaranteed Stop Loss Orders.
     * <p>
     *
     * @param guaranteedExecutionFees the Guaranteed Execution Fees as an
     *                                AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setGuaranteedExecutionFees(AccountUnits guaranteedExecutionFees) {
        this.guaranteedExecutionFees = guaranteedExecutionFees;
        return this;
    }

    /**
     * Get the Margin Rate
     * <p>
     * Client-provided margin rate override for the Account. The effective
     * margin rate of the Account is the lesser of this value and the OANDA
     * margin rate for the Account's division. This value is only provided if a
     * margin rate override exists for the Account.
     * <p>
     *
     * @return the Margin Rate
     * @see DecimalNumber
     */
    public DecimalNumber getMarginRate() {
        return this.marginRate;
    }

    /**
     * Set the Margin Rate
     * <p>
     * Client-provided margin rate override for the Account. The effective
     * margin rate of the Account is the lesser of this value and the OANDA
     * margin rate for the Account's division. This value is only provided if a
     * margin rate override exists for the Account.
     * <p>
     *
     * @param marginRate the Margin Rate as a DecimalNumber
     * @return {@link Account Account}
     * @see DecimalNumber
     */
    public Account setMarginRate(DecimalNumber marginRate) {
        this.marginRate = marginRate;
        return this;
    }

    /**
     * Get the Margin Call Enter Time
     * <p>
     * The date/time when the Account entered a margin call state. Only
     * provided if the Account is in a margin call.
     * <p>
     *
     * @return the Margin Call Enter Time
     * @see DateTime
     */
    public DateTime getMarginCallEnterTime() {
        return this.marginCallEnterTime;
    }

    /**
     * Set the Margin Call Enter Time
     * <p>
     * The date/time when the Account entered a margin call state. Only
     * provided if the Account is in a margin call.
     * <p>
     *
     * @param marginCallEnterTime the Margin Call Enter Time as a DateTime
     * @return {@link Account Account}
     * @see DateTime
     */
    public Account setMarginCallEnterTime(DateTime marginCallEnterTime) {
        this.marginCallEnterTime = marginCallEnterTime;
        return this;
    }

    /**
     * Get the Margin Call Extension Count
     * <p>
     * The number of times that the Account's current margin call was extended.
     * <p>
     *
     * @return the Margin Call Extension Count
     */
    public Long getMarginCallExtensionCount() {
        return this.marginCallExtensionCount;
    }

    /**
     * Set the Margin Call Extension Count
     * <p>
     * The number of times that the Account's current margin call was extended.
     * <p>
     *
     * @param marginCallExtensionCount the Margin Call Extension Count as a
     *                                 Long
     * @return {@link Account Account}
     */
    public Account setMarginCallExtensionCount(Long marginCallExtensionCount) {
        this.marginCallExtensionCount = marginCallExtensionCount;
        return this;
    }

    /**
     * Get the Last Margin Call Extension Time
     * <p>
     * The date/time of the Account's last margin call extension.
     * <p>
     *
     * @return the Last Margin Call Extension Time
     * @see DateTime
     */
    public DateTime getLastMarginCallExtensionTime() {
        return this.lastMarginCallExtensionTime;
    }

    /**
     * Set the Last Margin Call Extension Time
     * <p>
     * The date/time of the Account's last margin call extension.
     * <p>
     *
     * @param lastMarginCallExtensionTime the Last Margin Call Extension Time
     *                                    as a DateTime
     * @return {@link Account Account}
     * @see DateTime
     */
    public Account setLastMarginCallExtensionTime(DateTime lastMarginCallExtensionTime) {
        this.lastMarginCallExtensionTime = lastMarginCallExtensionTime;
        return this;
    }

    /**
     * Get the Open Trade Count
     * <p>
     * The number of Trades currently open in the Account.
     * <p>
     *
     * @return the Open Trade Count
     */
    public Long getOpenTradeCount() {
        return this.openTradeCount;
    }

    /**
     * Set the Open Trade Count
     * <p>
     * The number of Trades currently open in the Account.
     * <p>
     *
     * @param openTradeCount the Open Trade Count as a Long
     * @return {@link Account Account}
     */
    public Account setOpenTradeCount(Long openTradeCount) {
        this.openTradeCount = openTradeCount;
        return this;
    }

    /**
     * Get the Open Position Count
     * <p>
     * The number of Positions currently open in the Account.
     * <p>
     *
     * @return the Open Position Count
     */
    public Long getOpenPositionCount() {
        return this.openPositionCount;
    }

    /**
     * Set the Open Position Count
     * <p>
     * The number of Positions currently open in the Account.
     * <p>
     *
     * @param openPositionCount the Open Position Count as a Long
     * @return {@link Account Account}
     */
    public Account setOpenPositionCount(Long openPositionCount) {
        this.openPositionCount = openPositionCount;
        return this;
    }

    /**
     * Get the Pending Order Count
     * <p>
     * The number of Orders currently pending in the Account.
     * <p>
     *
     * @return the Pending Order Count
     */
    public Long getPendingOrderCount() {
        return this.pendingOrderCount;
    }

    /**
     * Set the Pending Order Count
     * <p>
     * The number of Orders currently pending in the Account.
     * <p>
     *
     * @param pendingOrderCount the Pending Order Count as a Long
     * @return {@link Account Account}
     */
    public Account setPendingOrderCount(Long pendingOrderCount) {
        this.pendingOrderCount = pendingOrderCount;
        return this;
    }

    /**
     * Get the Hedging Enabled
     * <p>
     * Flag indicating that the Account has hedging enabled.
     * <p>
     *
     * @return the Hedging Enabled
     */
    public Boolean getHedgingEnabled() {
        return this.hedgingEnabled;
    }

    /**
     * Set the Hedging Enabled
     * <p>
     * Flag indicating that the Account has hedging enabled.
     * <p>
     *
     * @param hedgingEnabled the Hedging Enabled as a Boolean
     * @return {@link Account Account}
     */
    public Account setHedgingEnabled(Boolean hedgingEnabled) {
        this.hedgingEnabled = hedgingEnabled;
        return this;
    }

    /**
     * Get the Last Order Fill timestamp.
     * <p>
     * The date/time of the last order that was filled for this account.
     * <p>
     *
     * @return the Last Order Fill timestamp.
     * @see DateTime
     */
    public DateTime getLastOrderFillTimestamp() {
        return this.lastOrderFillTimestamp;
    }

    /**
     * Set the Last Order Fill timestamp.
     * <p>
     * The date/time of the last order that was filled for this account.
     * <p>
     *
     * @param lastOrderFillTimestamp the Last Order Fill timestamp. as a
     *                               DateTime
     * @return {@link Account Account}
     * @see DateTime
     */
    public Account setLastOrderFillTimestamp(DateTime lastOrderFillTimestamp) {
        this.lastOrderFillTimestamp = lastOrderFillTimestamp;
        return this;
    }

    /**
     * Get the Unrealized Profit/Loss
     * <p>
     * The total unrealized profit/loss for all Trades currently open in the
     * Account.
     * <p>
     *
     * @return the Unrealized Profit/Loss
     * @see AccountUnits
     */
    public AccountUnits getUnrealizedPL() {
        return this.unrealizedPL;
    }

    /**
     * Set the Unrealized Profit/Loss
     * <p>
     * The total unrealized profit/loss for all Trades currently open in the
     * Account.
     * <p>
     *
     * @param unrealizedPL the Unrealized Profit/Loss as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setUnrealizedPL(AccountUnits unrealizedPL) {
        this.unrealizedPL = unrealizedPL;
        return this;
    }

    /**
     * Get the Net Asset Value
     * <p>
     * The net asset value of the Account. Equal to Account balance +
     * unrealizedPL.
     * <p>
     *
     * @return the Net Asset Value
     * @see AccountUnits
     */
    public AccountUnits getNAV() {
        return this.nAV;
    }

    /**
     * Set the Net Asset Value
     * <p>
     * The net asset value of the Account. Equal to Account balance +
     * unrealizedPL.
     * <p>
     *
     * @param nAV the Net Asset Value as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setNAV(AccountUnits nAV) {
        this.nAV = nAV;
        return this;
    }

    /**
     * Get the Margin Used
     * <p>
     * Margin currently used for the Account.
     * <p>
     *
     * @return the Margin Used
     * @see AccountUnits
     */
    public AccountUnits getMarginUsed() {
        return this.marginUsed;
    }

    /**
     * Set the Margin Used
     * <p>
     * Margin currently used for the Account.
     * <p>
     *
     * @param marginUsed the Margin Used as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginUsed(AccountUnits marginUsed) {
        this.marginUsed = marginUsed;
        return this;
    }

    /**
     * Get the Margin Available
     * <p>
     * Margin available for Account currency.
     * <p>
     *
     * @return the Margin Available
     * @see AccountUnits
     */
    public AccountUnits getMarginAvailable() {
        return this.marginAvailable;
    }

    /**
     * Set the Margin Available
     * <p>
     * Margin available for Account currency.
     * <p>
     *
     * @param marginAvailable the Margin Available as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginAvailable(AccountUnits marginAvailable) {
        this.marginAvailable = marginAvailable;
        return this;
    }

    /**
     * Get the Position Value
     * <p>
     * The value of the Account's open positions represented in the Account's
     * home currency.
     * <p>
     *
     * @return the Position Value
     * @see AccountUnits
     */
    public AccountUnits getPositionValue() {
        return this.positionValue;
    }

    /**
     * Set the Position Value
     * <p>
     * The value of the Account's open positions represented in the Account's
     * home currency.
     * <p>
     *
     * @param positionValue the Position Value as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setPositionValue(AccountUnits positionValue) {
        this.positionValue = positionValue;
        return this;
    }

    /**
     * Get the Closeout UPL
     * <p>
     * The Account's margin closeout unrealized PL.
     * <p>
     *
     * @return the Closeout UPL
     * @see AccountUnits
     */
    public AccountUnits getMarginCloseoutUnrealizedPL() {
        return this.marginCloseoutUnrealizedPL;
    }

    /**
     * Set the Closeout UPL
     * <p>
     * The Account's margin closeout unrealized PL.
     * <p>
     *
     * @param marginCloseoutUnrealizedPL the Closeout UPL as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginCloseoutUnrealizedPL(AccountUnits marginCloseoutUnrealizedPL) {
        this.marginCloseoutUnrealizedPL = marginCloseoutUnrealizedPL;
        return this;
    }

    /**
     * Get the Closeout NAV
     * <p>
     * The Account's margin closeout NAV.
     * <p>
     *
     * @return the Closeout NAV
     * @see AccountUnits
     */
    public AccountUnits getMarginCloseoutNAV() {
        return this.marginCloseoutNAV;
    }

    /**
     * Set the Closeout NAV
     * <p>
     * The Account's margin closeout NAV.
     * <p>
     *
     * @param marginCloseoutNAV the Closeout NAV as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginCloseoutNAV(AccountUnits marginCloseoutNAV) {
        this.marginCloseoutNAV = marginCloseoutNAV;
        return this;
    }

    /**
     * Get the Closeout Margin Used
     * <p>
     * The Account's margin closeout margin used.
     * <p>
     *
     * @return the Closeout Margin Used
     * @see AccountUnits
     */
    public AccountUnits getMarginCloseoutMarginUsed() {
        return this.marginCloseoutMarginUsed;
    }

    /**
     * Set the Closeout Margin Used
     * <p>
     * The Account's margin closeout margin used.
     * <p>
     *
     * @param marginCloseoutMarginUsed the Closeout Margin Used as an
     *                                 AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginCloseoutMarginUsed(AccountUnits marginCloseoutMarginUsed) {
        this.marginCloseoutMarginUsed = marginCloseoutMarginUsed;
        return this;
    }

    /**
     * Get the Margin Closeout Percentage
     * <p>
     * The Account's margin closeout percentage. When this value is 1.0 or
     * above the Account is in a margin closeout situation.
     * <p>
     *
     * @return the Margin Closeout Percentage
     * @see DecimalNumber
     */
    public DecimalNumber getMarginCloseoutPercent() {
        return this.marginCloseoutPercent;
    }

    /**
     * Set the Margin Closeout Percentage
     * <p>
     * The Account's margin closeout percentage. When this value is 1.0 or
     * above the Account is in a margin closeout situation.
     * <p>
     *
     * @param marginCloseoutPercent the Margin Closeout Percentage as a
     *                              DecimalNumber
     * @return {@link Account Account}
     * @see DecimalNumber
     */
    public Account setMarginCloseoutPercent(DecimalNumber marginCloseoutPercent) {
        this.marginCloseoutPercent = marginCloseoutPercent;
        return this;
    }

    /**
     * Get the Margin Closeout Position Value
     * <p>
     * The value of the Account's open positions as used for margin closeout
     * calculations represented in the Account's home currency.
     * <p>
     *
     * @return the Margin Closeout Position Value
     * @see DecimalNumber
     */
    public DecimalNumber getMarginCloseoutPositionValue() {
        return this.marginCloseoutPositionValue;
    }

    /**
     * Set the Margin Closeout Position Value
     * <p>
     * The value of the Account's open positions as used for margin closeout
     * calculations represented in the Account's home currency.
     * <p>
     *
     * @param marginCloseoutPositionValue the Margin Closeout Position Value as
     *                                    a DecimalNumber
     * @return {@link Account Account}
     * @see DecimalNumber
     */
    public Account setMarginCloseoutPositionValue(DecimalNumber marginCloseoutPositionValue) {
        this.marginCloseoutPositionValue = marginCloseoutPositionValue;
        return this;
    }

    /**
     * Get the Withdrawal Limit
     * <p>
     * The current WithdrawalLimit for the account which will be zero or a
     * positive value indicating how much can be withdrawn from the account.
     * <p>
     *
     * @return the Withdrawal Limit
     * @see AccountUnits
     */
    public AccountUnits getWithdrawalLimit() {
        return this.withdrawalLimit;
    }

    /**
     * Set the Withdrawal Limit
     * <p>
     * The current WithdrawalLimit for the account which will be zero or a
     * positive value indicating how much can be withdrawn from the account.
     * <p>
     *
     * @param withdrawalLimit the Withdrawal Limit as an AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setWithdrawalLimit(AccountUnits withdrawalLimit) {
        this.withdrawalLimit = withdrawalLimit;
        return this;
    }

    /**
     * Get the Margin Call Margin Used
     * <p>
     * The Account's margin call margin used.
     * <p>
     *
     * @return the Margin Call Margin Used
     * @see AccountUnits
     */
    public AccountUnits getMarginCallMarginUsed() {
        return this.marginCallMarginUsed;
    }

    /**
     * Set the Margin Call Margin Used
     * <p>
     * The Account's margin call margin used.
     * <p>
     *
     * @param marginCallMarginUsed the Margin Call Margin Used as an
     *                             AccountUnits
     * @return {@link Account Account}
     * @see AccountUnits
     */
    public Account setMarginCallMarginUsed(AccountUnits marginCallMarginUsed) {
        this.marginCallMarginUsed = marginCallMarginUsed;
        return this;
    }

    /**
     * Get the Margin Call Percentage
     * <p>
     * The Account's margin call percentage. When this value is 1.0 or above
     * the Account is in a margin call situation.
     * <p>
     *
     * @return the Margin Call Percentage
     * @see DecimalNumber
     */
    public DecimalNumber getMarginCallPercent() {
        return this.marginCallPercent;
    }

    /**
     * Set the Margin Call Percentage
     * <p>
     * The Account's margin call percentage. When this value is 1.0 or above
     * the Account is in a margin call situation.
     * <p>
     *
     * @param marginCallPercent the Margin Call Percentage as a DecimalNumber
     * @return {@link Account Account}
     * @see DecimalNumber
     */
    public Account setMarginCallPercent(DecimalNumber marginCallPercent) {
        this.marginCallPercent = marginCallPercent;
        return this;
    }

    /**
     * Get the Last Transaction ID
     * <p>
     * The ID of the last Transaction created for the Account.
     * <p>
     *
     * @return the Last Transaction ID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }

    /**
     * Set the Last Transaction ID
     * <p>
     * The ID of the last Transaction created for the Account.
     * <p>
     *
     * @param lastTransactionID the Last Transaction ID as a TransactionID
     * @return {@link Account Account}
     * @see TransactionID
     */
    public Account setLastTransactionID(TransactionID lastTransactionID) {
        this.lastTransactionID = lastTransactionID;
        return this;
    }

    /**
     * Get the Open Trades
     * <p>
     * The details of the Trades currently open in the Account.
     * <p>
     *
     * @return the Open Trades
     * @see TradeSummary
     */
    public List<TradeSummary> getTrades() {
        return this.trades;
    }

    /**
     * Set the Open Trades
     * <p>
     * The details of the Trades currently open in the Account.
     * <p>
     *
     * @param trades the Open Trades
     * @return {@link Account Account}
     * @see TradeSummary
     */
    public Account setTrades(List<TradeSummary> trades) {
        this.trades = trades;
        return this;
    }

    /**
     * Get the Positions
     * <p>
     * The details all Account Positions.
     * <p>
     *
     * @return the Positions
     * @see Position
     */
    public List<Position> getPositions() {
        return this.positions;
    }

    /**
     * Set the Positions
     * <p>
     * The details all Account Positions.
     * <p>
     *
     * @param positions the Positions
     * @return {@link Account Account}
     * @see Position
     */
    public Account setPositions(List<Position> positions) {
        this.positions = positions;
        return this;
    }

    /**
     * Get the Pending Orders
     * <p>
     * The details of the Orders currently pending in the Account.
     * <p>
     *
     * @return the Pending Orders
     * @see Order
     */
    public List<Order> getOrders() {
        return this.orders;
    }

    /**
     * Set the Pending Orders
     * <p>
     * The details of the Orders currently pending in the Account.
     * <p>
     *
     * @param orders the Pending Orders
     * @return {@link Account Account}
     * @see Order
     */
    public Account setOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    @Override
    public String toString() {
        return "Account(" +
                "id=" +
                (id == null ? "null" : id.toString()) + ", " +
                "alias=" +
                (alias == null ? "null" : alias) + ", " +
                "currency=" +
                (currency == null ? "null" : currency.toString()) + ", " +
                "balance=" +
                (balance == null ? "null" : balance.toString()) + ", " +
                "createdByUserID=" +
                (createdByUserID == null ? "null" : createdByUserID.toString()) + ", " +
                "createdTime=" +
                (createdTime == null ? "null" : createdTime.toString()) + ", " +
                "guaranteedStopLossOrderMode=" +
                (guaranteedStopLossOrderMode == null ? "null" : guaranteedStopLossOrderMode.toString()) + ", " +
                "pl=" +
                (pl == null ? "null" : pl.toString()) + ", " +
                "resettablePL=" +
                (resettablePL == null ? "null" : resettablePL.toString()) + ", " +
                "resettablePLTime=" +
                (resettablePLTime == null ? "null" : resettablePLTime.toString()) + ", " +
                "financing=" +
                (financing == null ? "null" : financing.toString()) + ", " +
                "commission=" +
                (commission == null ? "null" : commission.toString()) + ", " +
                "guaranteedExecutionFees=" +
                (guaranteedExecutionFees == null ? "null" : guaranteedExecutionFees.toString()) + ", " +
                "marginRate=" +
                (marginRate == null ? "null" : marginRate.toString()) + ", " +
                "marginCallEnterTime=" +
                (marginCallEnterTime == null ? "null" : marginCallEnterTime.toString()) + ", " +
                "marginCallExtensionCount=" +
                (marginCallExtensionCount == null ? "null" : marginCallExtensionCount.toString()) + ", " +
                "lastMarginCallExtensionTime=" +
                (lastMarginCallExtensionTime == null ? "null" : lastMarginCallExtensionTime.toString()) + ", " +
                "openTradeCount=" +
                (openTradeCount == null ? "null" : openTradeCount.toString()) + ", " +
                "openPositionCount=" +
                (openPositionCount == null ? "null" : openPositionCount.toString()) + ", " +
                "pendingOrderCount=" +
                (pendingOrderCount == null ? "null" : pendingOrderCount.toString()) + ", " +
                "hedgingEnabled=" +
                (hedgingEnabled == null ? "null" : hedgingEnabled.toString()) + ", " +
                "lastOrderFillTimestamp=" +
                (lastOrderFillTimestamp == null ? "null" : lastOrderFillTimestamp.toString()) + ", " +
                "unrealizedPL=" +
                (unrealizedPL == null ? "null" : unrealizedPL.toString()) + ", " +
                "NAV=" +
                (nAV == null ? "null" : nAV.toString()) + ", " +
                "marginUsed=" +
                (marginUsed == null ? "null" : marginUsed.toString()) + ", " +
                "marginAvailable=" +
                (marginAvailable == null ? "null" : marginAvailable.toString()) + ", " +
                "positionValue=" +
                (positionValue == null ? "null" : positionValue.toString()) + ", " +
                "marginCloseoutUnrealizedPL=" +
                (marginCloseoutUnrealizedPL == null ? "null" : marginCloseoutUnrealizedPL.toString()) + ", " +
                "marginCloseoutNAV=" +
                (marginCloseoutNAV == null ? "null" : marginCloseoutNAV.toString()) + ", " +
                "marginCloseoutMarginUsed=" +
                (marginCloseoutMarginUsed == null ? "null" : marginCloseoutMarginUsed.toString()) + ", " +
                "marginCloseoutPercent=" +
                (marginCloseoutPercent == null ? "null" : marginCloseoutPercent.toString()) + ", " +
                "marginCloseoutPositionValue=" +
                (marginCloseoutPositionValue == null ? "null" : marginCloseoutPositionValue.toString()) + ", " +
                "withdrawalLimit=" +
                (withdrawalLimit == null ? "null" : withdrawalLimit.toString()) + ", " +
                "marginCallMarginUsed=" +
                (marginCallMarginUsed == null ? "null" : marginCallMarginUsed.toString()) + ", " +
                "marginCallPercent=" +
                (marginCallPercent == null ? "null" : marginCallPercent.toString()) + ", " +
                "lastTransactionID=" +
                (lastTransactionID == null ? "null" : lastTransactionID.toString()) + ", " +
                "trades=" +
                (trades == null ? "null" : trades.toString()) + ", " +
                "positions=" +
                (positions == null ? "null" : positions.toString()) + ", " +
                "orders=" +
                (orders == null ? "null" : orders.toString()) +
                ")";
    }
}
