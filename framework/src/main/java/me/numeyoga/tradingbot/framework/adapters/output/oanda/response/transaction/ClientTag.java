package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * A client-provided tag that can contain any data and may be assigned to their
 * Orders or Trades. Tags are typically used to associate groups of Trades
 * and/or Orders together.
 */
@JsonbTypeAdapter(ClientTag.JsonAdapter.class)
public class ClientTag extends StringPrimitive {

    /**
     * ClientTag copy constructor.
     * <p>
     * @param clientTag the 
     */
    public ClientTag(ClientTag clientTag)
    {
        super(clientTag.toString());
    }

    /**
     * ClientTag constructor.
     * <p>
     * @param clientTag the ClientTag as a String
     */
    public ClientTag(String clientTag) {
        super(clientTag);
    }

    /**
     * JSON adapter for reading and writing ClientTag0
     */
    public static class JsonAdapter implements JsonbAdapter<ClientTag, String> {

        @Override
        public String adaptToJson(ClientTag clientTag) {
            return clientTag.toString();
        }

        @Override
        public ClientTag adaptFromJson(String s) {
            return new ClientTag(s);
        }
    }
}
