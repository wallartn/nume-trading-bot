package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common;

import java.math.BigDecimal;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The string representation of a Price for a Bucket.
 * <p>
 * A decimal number encodes as a string. The amount of precision provided
 * depends on the Instrument.
 */
@JsonbTypeAdapter(PriceValue.JsonAdapter.class)
public class PriceValue extends StringPrimitive {

    /**
     * PriceValue copy constructor.
     * <p>
     * @param priceValue the 
     */
    public PriceValue(PriceValue priceValue)
    {
        super(priceValue.toString());
    }

    /**
     * PriceValue constructor.
     * <p>
     * @param priceValue the PriceValue as a String
     */
    public PriceValue(String priceValue) {
        super(priceValue);
    }

    /**
     * PriceValue constructor.
     * <p>
     * @param priceValue the PriceValue as a double
     */
    public PriceValue(double priceValue) {
        super(String.valueOf(priceValue));
    }

    /**
     * PriceValue constructor.
     * <p>
     * @param priceValue the PriceValue as a BigDecimal
     */
    public PriceValue(BigDecimal priceValue) {
        super(priceValue.toPlainString());
    }

    /**
     * Get the PriceValue
     * <p>
     * The string representation of a Price for a Bucket.
     * <p>
     * @return the PriceValue as a double
     * @see PriceValue
     */
    public double doubleValue() {
        return Double.valueOf(this.string);
    }

    /**
     * Get the PriceValue
     * <p>
     * The string representation of a Price for a Bucket.
     * <p>
     * @return the PriceValue as a BigDecimal
     * @see PriceValue
     */
    public BigDecimal bigDecimalValue() {
        return new BigDecimal(this.string);
    }

    /**
     * JSON adapter for reading and writing PriceValue0
     */
    public static class JsonAdapter implements JsonbAdapter<PriceValue, String> {

        @Override
        public String adaptToJson(PriceValue priceValue) {
            return priceValue.toString();
        }

        @Override
        public PriceValue adaptFromJson(String s) {
            return new PriceValue(s);
        }
    }
}
