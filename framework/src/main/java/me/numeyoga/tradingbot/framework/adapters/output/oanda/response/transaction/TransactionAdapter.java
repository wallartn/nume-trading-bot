package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import java.lang.reflect.Type;

public class TransactionAdapter implements JsonbDeserializer<Transaction> {

    private final Jsonb jsonb = JsonbBuilder.create();

    @Override
    public Transaction deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Type type) {
        JsonObject jsonObject = jsonParser.getObject();
        String typeString = jsonObject.getString("type");
        TransactionType objType = TransactionType.valueOf(typeString);

        switch (objType) {
            case MARKET_ORDER:
                return jsonb.fromJson(jsonObject.toString(), MarketOrderTransaction.class);
            case ORDER_FILL:
                return jsonb.fromJson(jsonObject.toString(), OrderFillTransaction.class);
            case ORDER_CANCEL:
                return jsonb.fromJson(jsonObject.toString(), OrderCancelTransaction.class);
            case MARKET_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), MarketOrderRejectTransaction.class);
            case TRADE_CLIENT_EXTENSIONS_MODIFY:
                return jsonb.fromJson(jsonObject.toString(), TradeClientExtensionsModifyTransaction.class);
            case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT:
                return jsonb.fromJson(jsonObject.toString(), TradeClientExtensionsModifyRejectTransaction.class);
            case TAKE_PROFIT_ORDER:
                return jsonb.fromJson(jsonObject.toString(), TakeProfitOrderTransaction.class);
            case STOP_LOSS_ORDER:
                return jsonb.fromJson(jsonObject.toString(), StopLossOrderTransaction.class);
            case TRAILING_STOP_LOSS_ORDER:
                return jsonb.fromJson(jsonObject.toString(), TrailingStopLossOrderTransaction.class);
            case ORDER_CANCEL_REJECT:
                return jsonb.fromJson(jsonObject.toString(), OrderCancelRejectTransaction.class);
            case TAKE_PROFIT_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), TakeProfitOrderRejectTransaction.class);
            case STOP_LOSS_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), StopLossOrderRejectTransaction.class);
            case TRAILING_STOP_LOSS_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), TrailingStopLossOrderRejectTransaction.class);
            case CLIENT_CONFIGURE:
                return jsonb.fromJson(jsonObject.toString(), ClientConfigureTransaction.class);
            case CLIENT_CONFIGURE_REJECT:
                return jsonb.fromJson(jsonObject.toString(), ClientConfigureRejectTransaction.class);
            case CREATE:
                return jsonb.fromJson(jsonObject.toString(), CreateTransaction.class);
            case CLOSE:
                return jsonb.fromJson(jsonObject.toString(), CloseTransaction.class);
            case REOPEN:
                return jsonb.fromJson(jsonObject.toString(), ReopenTransaction.class);
            case TRANSFER_FUNDS:
                return jsonb.fromJson(jsonObject.toString(), TransferFundsTransaction.class);
            case TRANSFER_FUNDS_REJECT:
                return jsonb.fromJson(jsonObject.toString(), TransferFundsRejectTransaction.class);
            case FIXED_PRICE_ORDER:
                return jsonb.fromJson(jsonObject.toString(), FixedPriceOrderTransaction.class);
            case LIMIT_ORDER:
                return jsonb.fromJson(jsonObject.toString(), LimitOrderTransaction.class);
            case LIMIT_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), LimitOrderRejectTransaction.class);
            case STOP_ORDER:
                return jsonb.fromJson(jsonObject.toString(), StopOrderTransaction.class);
            case STOP_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), StopOrderRejectTransaction.class);
            case MARKET_IF_TOUCHED_ORDER:
                return jsonb.fromJson(jsonObject.toString(), MarketIfTouchedOrderTransaction.class);
            case MARKET_IF_TOUCHED_ORDER_REJECT:
                return jsonb.fromJson(jsonObject.toString(), MarketIfTouchedOrderRejectTransaction.class);
            case ORDER_CLIENT_EXTENSIONS_MODIFY:
                return jsonb.fromJson(jsonObject.toString(), OrderClientExtensionsModifyTransaction.class);
            case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT:
                return jsonb.fromJson(jsonObject.toString(), OrderClientExtensionsModifyRejectTransaction.class);
            case MARGIN_CALL_ENTER:
                return jsonb.fromJson(jsonObject.toString(), MarginCallEnterTransaction.class);
            case MARGIN_CALL_EXTEND:
                return jsonb.fromJson(jsonObject.toString(), MarginCallExtendTransaction.class);
            case MARGIN_CALL_EXIT:
                return jsonb.fromJson(jsonObject.toString(), MarginCallExitTransaction.class);
            case DELAYED_TRADE_CLOSURE:
                return jsonb.fromJson(jsonObject.toString(), DelayedTradeClosureTransaction.class);
            case DAILY_FINANCING:
                return jsonb.fromJson(jsonObject.toString(), DailyFinancingTransaction.class);
            case RESET_RESETTABLE_PL:
                return jsonb.fromJson(jsonObject.toString(), ResetResettablePLTransaction.class);
        }
        return null;
    }
}
