package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common.Price;

/**
 * PricingBasePricesResponse
 */
public class PricingBasePricesResponse {

    /**
     * PricingBasePricesResponse Constructor
     * <p>
     * Construct a new PricingBasePricesResponse
     */
    private PricingBasePricesResponse() {
    }

    @JsonbProperty("prices") private ArrayList<Price> prices;

    /**
     * Get the prices
     * <p>
     * The list of prices that satisfy the request.
     * <p>
     * @return the prices
     * @see Price
     */
    public List<Price> getPrices() {
        return this.prices;
    }
}
