package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.instrument;

import javax.json.bind.annotation.JsonbProperty;

/**
 * InstrumentPositionBookResponse
 */
public class InstrumentPositionBookResponse {

    /**
     * InstrumentPositionBookResponse Constructor
     * <p>
     * Construct a new InstrumentPositionBookResponse
     */
    private InstrumentPositionBookResponse() {
    }

    @JsonbProperty("positionBook") private PositionBook positionBook;

    /**
     * Get the positionBook
     * <p>
     * The instrument's position book
     * <p>
     * @return the positionBook
     * @see PositionBook
     */
    public PositionBook getPositionBook() {
        return this.positionBook;
    }
}
