package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * A client-provided comment that can contain any data and may be assigned to
 * their Orders or Trades. Comments are typically used to provide extra context
 * or meaning to an Order or Trade.
 */
@JsonbTypeAdapter(ClientComment.JsonAdapter.class)
public class ClientComment extends StringPrimitive {

    /**
     * ClientComment copy constructor.
     * <p>
     * @param clientComment the 
     */
    public ClientComment(ClientComment clientComment)
    {
        super(clientComment.toString());
    }

    /**
     * ClientComment constructor.
     * <p>
     * @param clientComment the ClientComment as a String
     */
    public ClientComment(String clientComment) {
        super(clientComment);
    }

    /**
     * JSON adapter for reading and writing ClientComment0
     */
    public static class JsonAdapter implements JsonbAdapter<ClientComment, String> {
        @Override
        public String adaptToJson(ClientComment clientComment) {
            return clientComment.toString();
        }

        @Override
        public ClientComment adaptFromJson(String s) {
            return new ClientComment(s);
        }
    }
}
