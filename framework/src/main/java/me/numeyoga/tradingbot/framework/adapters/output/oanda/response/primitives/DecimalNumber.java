package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.math.BigDecimal;

/**
 * The string representation of a decimal number.
 * <p>
 * A decimal number encoded as a string. The amount of precision provided
 * depends on what the number represents.
 */
@JsonbTypeAdapter(DecimalNumber.JsonAdapter.class)
public class DecimalNumber extends StringPrimitive {

    /**
     * DecimalNumber copy constructor.
     * <p>
     * @param decimalNumber the 
     */
    public DecimalNumber(DecimalNumber decimalNumber)
    {
        super(decimalNumber.toString());
    }

    /**
     * DecimalNumber constructor.
     * <p>
     * @param decimalNumber the DecimalNumber as a String
     */
    public DecimalNumber(String decimalNumber) {
        super(decimalNumber);
    }

    /**
     * DecimalNumber constructor.
     * <p>
     * @param decimalNumber the DecimalNumber as a double
     */
    public DecimalNumber(double decimalNumber) {
        super(String.valueOf(decimalNumber));
    }

    /**
     * DecimalNumber constructor.
     * <p>
     * @param decimalNumber the DecimalNumber as a BigDecimal
     */
    public DecimalNumber(BigDecimal decimalNumber) {
        super(decimalNumber.toPlainString());
    }

    /**
     * Get the DecimalNumber
     * <p>
     * The string representation of a decimal number.
     * <p>
     * @return the DecimalNumber as a double
     * @see DecimalNumber
     */
    public double doubleValue() {
        return Double.valueOf(this.string);
    }

    /**
     * Get the DecimalNumber
     * <p>
     * The string representation of a decimal number.
     * <p>
     * @return the DecimalNumber as a BigDecimal
     * @see DecimalNumber
     */
    public BigDecimal bigDecimalValue() {
        return new BigDecimal(this.string);
    }

    /**
     * JSON adapter for reading and writing DecimalNumber0
     */
    public static class JsonAdapter implements JsonbAdapter<DecimalNumber, String> {

        @Override
        public String adaptToJson(DecimalNumber decimalNumber) {
            return decimalNumber.toString();
        }

        @Override
        public DecimalNumber adaptFromJson(String s) {
            return new DecimalNumber(s);
        }
    }
}
