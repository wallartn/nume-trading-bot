package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.Request;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountID;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.ClientExtensions;

/**
 * OrderSetClientExtensionsRequest
 */
public class OrderSetClientExtensionsRequest extends Request {

    private static class Body {
        @JsonbProperty("clientExtensions") private ClientExtensions clientExtensions;
        @JsonbProperty("tradeClientExtensions") private ClientExtensions tradeClientExtensions;
    }

    /**
     * OrderSetClientExtensionsRequest Constructor
     * <p>
     * Construct a new OrderSetClientExtensionsRequest
     * <p>
     * @param accountID Account Identifier
     * @param orderSpecifier The Order Specifier
     */
    public OrderSetClientExtensionsRequest(AccountID accountID, OrderSpecifier orderSpecifier) {
        this.body = new Body();
        this.setPathParam("accountID", accountID);
        this.setPathParam("orderSpecifier", orderSpecifier);

    }

    /**
     * Set the clientExtensions
     * <p>
     * The Client Extensions to update for the Order. Do not set, modify, or
     * delete clientExtensions if your account is associated with MT4.
     * <p>
     * @param clientExtensions the clientExtensions as a ClientExtensions
     * @return {@link OrderSetClientExtensionsRequest
     * OrderSetClientExtensionsRequest}
     * @see ClientExtensions
     */
    public OrderSetClientExtensionsRequest setClientExtensions(ClientExtensions clientExtensions)
    {
        ((Body) this.body).clientExtensions = clientExtensions;
        return this;
    }

    /**
     * Set the tradeClientExtensions
     * <p>
     * The Client Extensions to update for the Trade created when the Order is
     * filled. Do not set, modify, or delete clientExtensions if your account
     * is associated with MT4.
     * <p>
     * @param tradeClientExtensions the tradeClientExtensions as a
     * ClientExtensions
     * @return {@link OrderSetClientExtensionsRequest
     * OrderSetClientExtensionsRequest}
     * @see ClientExtensions
     */
    public OrderSetClientExtensionsRequest setTradeClientExtensions(ClientExtensions tradeClientExtensions)
    {
        ((Body) this.body).tradeClientExtensions = tradeClientExtensions;
        return this;
    }
}
