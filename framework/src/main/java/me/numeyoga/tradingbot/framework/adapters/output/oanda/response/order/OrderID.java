package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The Order's identifier, unique within the Order's Account.
 * <p>
 * The string representation of the OANDA-assigned OrderID. OANDA-assigned
 * OrderIDs are positive integers, and are derived from the TransactionID of
 * the Transaction that created the Order.
 */
@JsonbTypeAdapter(OrderID.JsonAdapter.class)
public class OrderID extends StringPrimitive {

    /**
     * OrderID copy constructor.
     * <p>
     * @param orderID the 
     */
    public OrderID(OrderID orderID)
    {
        super(orderID.toString());
    }

    /**
     * OrderID constructor.
     * <p>
     * @param orderID the OrderID as a String
     */
    public OrderID(String orderID) {
        super(orderID);
    }

    /**
     * Construct an OrderID from a TransactionID
     * <p>
     * @param transactionID the TransactionID to convert
     */
    public OrderID(TransactionID transactionID) {
        super(transactionID.toString());
    }

    /**
     * JSON adapter for reading and writing OrderID0
     */
    public static class JsonAdapter implements JsonbAdapter<OrderID, String> {

        @Override
        public String adaptToJson(OrderID orderID) {
            return orderID.toString();
        }

        @Override
        public OrderID adaptFromJson(String s) {
            return new OrderID(s);
        }
    }
}
