package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.instrument;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common.Price;

/**
 * InstrumentPriceResponse
 */
public class InstrumentPriceResponse {

    /**
     * InstrumentPriceResponse Constructor
     * <p>
     * Construct a new InstrumentPriceResponse
     */
    private InstrumentPriceResponse() {
    }

    @JsonbProperty("price") private Price price;

    /**
     * Get the price
     * <p>
     * The price that satisfies the request.
     * <p>
     * @return the price
     * @see Price
     */
    public Price getPrice() {
        return this.price;
    }
}
