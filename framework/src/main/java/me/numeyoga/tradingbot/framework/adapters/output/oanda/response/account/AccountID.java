package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The string representation of an Account Identifier.
 * <p>
 * "-"-delimited string with format
 * "{siteID}-{divisionID}-{userID}-{accountNumber}"
 */
@JsonbTypeAdapter(AccountID.JsonAdapter.class)
public class AccountID extends StringPrimitive {

    /**
     * AccountID copy constructor.
     * <p>
     *
     * @param accountID the
     */
    public AccountID(AccountID accountID) {
        super(accountID.toString());
    }

    /**
     * AccountID constructor.
     * <p>
     *
     * @param accountID the AccountID as a String
     */
    public AccountID(String accountID) {
        super(accountID);
    }

    /**
     * JSON adapter for reading and writing AccountID0
     */
    public static class JsonAdapter implements JsonbAdapter<AccountID, String> {

        @Override
        public String adaptToJson(AccountID accountID) throws Exception {
            return accountID.toString();
        }

        @Override
        public AccountID adaptFromJson(String s) throws Exception {
            return new AccountID(s);
        }
    }
}
