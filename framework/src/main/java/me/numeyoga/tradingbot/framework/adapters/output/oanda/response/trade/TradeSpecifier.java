package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The identification of a Trade as referred to by clients
 * <p>
 * Either the Trade's OANDA-assigned TradeID or the Trade's client-provided
 * ClientID prefixed by the "@" symbol
 */
@JsonbTypeAdapter(TradeSpecifier.JsonAdapter.class)
public class TradeSpecifier extends StringPrimitive {

    /**
     * TradeSpecifier copy constructor.
     * <p>
     * @param tradeSpecifier the 
     */
    public TradeSpecifier(TradeSpecifier tradeSpecifier)
    {
        super(tradeSpecifier.toString());
    }

    /**
     * TradeSpecifier constructor.
     * <p>
     * @param tradeSpecifier the TradeSpecifier as a String
     */
    public TradeSpecifier(String tradeSpecifier) {
        super(tradeSpecifier);
    }

    /**
     * Construct a TradeSpecifier from a TradeID
     * <p>
     * @param tradeID the TradeID to convert
     */
    public TradeSpecifier(TradeID tradeID) {
        super(tradeID.toString());
    }

    /**
     * Construct a TradeSpecifier from a TransactionID
     * <p>
     * @param transactionID the TransactionID to convert
     */
    public TradeSpecifier(TransactionID transactionID) {
        super(transactionID.toString());
    }

    /**
     * JSON adapter for reading and writing TradeSpecifier0
     */
    public static class JsonAdapter implements JsonbAdapter<TradeSpecifier, String> {

        @Override
        public String adaptToJson(TradeSpecifier tradeSpecifier) {
            return tradeSpecifier.toString();
        }

        @Override
        public TradeSpecifier adaptFromJson(String s) {
            return new TradeSpecifier(s);
        }
    }
}
