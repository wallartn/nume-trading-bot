package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.Instrument;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * AccountInstrumentsResponse
 */
public class AccountInstrumentsResponse {

    /**
     * AccountInstrumentsResponse Constructor
     * <p>
     * Construct a new AccountInstrumentsResponse
     */
    private AccountInstrumentsResponse() {
    }

    @JsonbProperty("instruments") private ArrayList<Instrument> instruments;

    /**
     * Get the instruments
     * <p>
     * The requested list of instruments.
     * <p>
     * @return the instruments
     * @see Instrument
     */
    public List<Instrument> getInstruments() {
        return this.instruments;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
