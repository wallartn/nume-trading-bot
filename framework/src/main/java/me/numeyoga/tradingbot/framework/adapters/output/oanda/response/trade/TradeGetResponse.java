package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * TradeGetResponse
 */
public class TradeGetResponse {

    /**
     * TradeGetResponse Constructor
     * <p>
     * Construct a new TradeGetResponse
     */
    private TradeGetResponse() {
    }

    @JsonbProperty("trade") private Trade trade;

    /**
     * Get the trade
     * <p>
     * The Account's list of open Trades
     * <p>
     * @return the trade
     * @see Trade
     */
    public Trade getTrade() {
        return this.trade;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
