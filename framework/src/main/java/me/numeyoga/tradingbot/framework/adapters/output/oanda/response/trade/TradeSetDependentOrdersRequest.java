package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.Request;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountID;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.NullableType;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.StopLossDetails;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TakeProfitDetails;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TrailingStopLossDetails;

/**
 * TradeSetDependentOrdersRequest
 */
public class TradeSetDependentOrdersRequest extends Request {

    private static class Body {
        @JsonbProperty("takeProfit") private NullableType<TakeProfitDetails> takeProfit;
        @JsonbProperty("stopLoss") private NullableType<StopLossDetails> stopLoss;
        @JsonbProperty("trailingStopLoss") private NullableType<TrailingStopLossDetails> trailingStopLoss;
    }

    /**
     * TradeSetDependentOrdersRequest Constructor
     * <p>
     * Construct a new TradeSetDependentOrdersRequest
     * <p>
     * @param accountID Account Identifier
     * @param tradeSpecifier Specifier for the Trade
     */
    public TradeSetDependentOrdersRequest(AccountID accountID, TradeSpecifier tradeSpecifier) {
        this.body = new Body();
        this.setPathParam("accountID", accountID);
        this.setPathParam("tradeSpecifier", tradeSpecifier);

    }

    /**
     * Set the takeProfit
     * <p>
     * The specification of the Take Profit to create/modify/cancel. If
     * takeProfit is set to null, the Take Profit Order will be cancelled if it
     * exists. If takeProfit is not provided, the exisiting Take Profit Order
     * will not be modified. If a sub-field of takeProfit is not specified,
     * that field will be set to a default value on create, and be inherited by
     * the replacing order on modify.
     * <p>
     * @param takeProfit the takeProfit as a TakeProfitDetails
     * @return {@link TradeSetDependentOrdersRequest
     * TradeSetDependentOrdersRequest}
     * @see TakeProfitDetails
     */
    public TradeSetDependentOrdersRequest setTakeProfit(TakeProfitDetails takeProfit)
    {
        ((Body) this.body).takeProfit = new NullableType<TakeProfitDetails>(takeProfit);
        return this;
    }

    /**
     * Set the stopLoss
     * <p>
     * The specification of the Stop Loss to create/modify/cancel. If stopLoss
     * is set to null, the Stop Loss Order will be cancelled if it exists. If
     * stopLoss is not provided, the exisiting Stop Loss Order will not be
     * modified. If a sub-field of stopLoss is not specified, that field will
     * be set to a default value on create, and be inherited by the replacing
     * order on modify.
     * <p>
     * @param stopLoss the stopLoss as a StopLossDetails
     * @return {@link TradeSetDependentOrdersRequest
     * TradeSetDependentOrdersRequest}
     * @see StopLossDetails
     */
    public TradeSetDependentOrdersRequest setStopLoss(StopLossDetails stopLoss)
    {
        ((Body) this.body).stopLoss = new NullableType<StopLossDetails>(stopLoss);
        return this;
    }

    /**
     * Set the trailingStopLoss
     * <p>
     * The specification of the Trailing Stop Loss to create/modify/cancel. If
     * trailingStopLoss is set to null, the Trailing Stop Loss Order will be
     * cancelled if it exists. If trailingStopLoss is not provided, the
     * exisiting Trailing Stop Loss Order will not be modified. If a sub-field
     * of trailngStopLoss is not specified, that field will be set to a default
     * value on create, and be inherited by the replacing order on modify.
     * <p>
     * @param trailingStopLoss the trailingStopLoss as a
     * TrailingStopLossDetails
     * @return {@link TradeSetDependentOrdersRequest
     * TradeSetDependentOrdersRequest}
     * @see TrailingStopLossDetails
     */
    public TradeSetDependentOrdersRequest setTrailingStopLoss(TrailingStopLossDetails trailingStopLoss)
    {
        ((Body) this.body).trailingStopLoss = new NullableType<TrailingStopLossDetails>(trailingStopLoss);
        return this;
    }
}
