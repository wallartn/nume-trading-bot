package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.trade;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The Trade's identifier, unique within the Trade's Account.
 * <p>
 * The string representation of the OANDA-assigned TradeID. OANDA-assigned
 * TradeIDs are positive integers, and are derived from the TransactionID of
 * the Transaction that opened the Trade.
 */
@JsonbTypeAdapter(TradeID.JsonAdapter.class)
public class TradeID extends StringPrimitive {

    /**
     * TradeID copy constructor.
     * <p>
     * @param tradeID the 
     */
    public TradeID(TradeID tradeID)
    {
        super(tradeID.toString());
    }

    /**
     * TradeID constructor.
     * <p>
     * @param tradeID the TradeID as a String
     */
    public TradeID(String tradeID) {
        super(tradeID);
    }

    /**
     * Construct a TradeID from a TransactionID
     * <p>
     * @param transactionID the TransactionID to convert
     */
    public TradeID(TransactionID transactionID) {
        super(transactionID.toString());
    }

    /**
     * JSON adapter for reading and writing TradeID0
     */
    public static class JsonAdapter implements JsonbAdapter<TradeID, String> {

        @Override
        public String adaptToJson(TradeID tradeID) {
            return tradeID.toString();
        }

        @Override
        public TradeID adaptFromJson(String s) {
            return new TradeID(s);
        }
    }
}
