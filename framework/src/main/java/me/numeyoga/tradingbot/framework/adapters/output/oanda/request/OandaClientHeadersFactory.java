package me.numeyoga.tradingbot.framework.adapters.output.oanda.request;

import io.quarkus.rest.client.reactive.ReactiveClientHeadersFactory;
import io.smallrye.mutiny.Uni;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.OandaConfig;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

@ApplicationScoped
public class OandaClientHeadersFactory extends ReactiveClientHeadersFactory {

    @Inject
    OandaConfig config;

    @Override
    public Uni<MultivaluedMap<String, String>> getHeaders(MultivaluedMap<String, String> incomingHeaders) {
        return Uni.createFrom().item(() -> {
            incomingHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + config.bearerToken());
            return incomingHeaders;
        });
    }
}
