package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import javax.json.bind.annotation.JsonbProperty;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Properties related to an Account.
 */
public class AccountProperties {

    @JsonbProperty("id")
    AccountID id;
    @JsonbProperty("mt4AccountID")
    Long mt4AccountID;
    @JsonbProperty("tags")
    List<String> tags;

    /**
     * Default constructor.
     */
    public AccountProperties() {
    }

    /**
     * Copy constructor
     * <p>
     *
     * @param other the AccountProperties to copy
     */
    public AccountProperties(AccountProperties other) {
        this.id = other.id;
        if (other.mt4AccountID != null) {
            this.mt4AccountID = other.mt4AccountID;
        }
        if (other.tags != null) {
            this.tags = new ArrayList<>(other.tags);
        }
    }

    /**
     * Get the ID
     * <p>
     * The Account's identifier
     * <p>
     *
     * @return the ID
     * @see AccountID
     */
    public AccountID getId() {
        return this.id;
    }

    /**
     * Set the ID
     * <p>
     * The Account's identifier
     * <p>
     *
     * @param id the ID as an AccountID
     * @return {@link AccountProperties AccountProperties}
     * @see AccountID
     */
    public AccountProperties setId(AccountID id) {
        this.id = id;
        return this;
    }

    /**
     * Get the MT4 Account ID
     * <p>
     * The Account's associated MT4 Account ID. This field will not be present
     * if the Account is not an MT4 account.
     * <p>
     *
     * @return the MT4 Account ID
     */
    public Long getMt4AccountID() {
        return this.mt4AccountID;
    }

    /**
     * Set the MT4 Account ID
     * <p>
     * The Account's associated MT4 Account ID. This field will not be present
     * if the Account is not an MT4 account.
     * <p>
     *
     * @param mt4AccountID the MT4 Account ID as a Long
     * @return {@link AccountProperties AccountProperties}
     */
    public AccountProperties setMt4AccountID(Long mt4AccountID) {
        this.mt4AccountID = mt4AccountID;
        return this;
    }

    /**
     * Get the Account Tags
     * <p>
     * The Account's tags
     * <p>
     *
     * @return the Account Tags
     */
    public List<String> getTags() {
        return this.tags;
    }

    /**
     * Set the Account Tags
     * <p>
     * The Account's tags
     * <p>
     *
     * @param tags the Account Tags
     * @return {@link AccountProperties AccountProperties}
     */
    public AccountProperties setTags(Collection<?> tags) {
        ArrayList<String> newTags = new ArrayList<>(tags.size());
        tags.forEach(item -> {
            if (item instanceof String sItem) {
                newTags.add(sItem);
            } else {
                throw new IllegalArgumentException(
                        item.getClass().getName() + " cannot be converted to a String"
                );
            }
        });
        this.tags = newTags;
        return this;
    }

    @Override
    public String toString() {
        return "AccountProperties(" +
                "id=" +
                (id == null ? "null" : id.toString()) + ", " +
                "mt4AccountID=" +
                (mt4AccountID == null ? "null" : mt4AccountID.toString()) + ", " +
                "tags=" +
                (tags == null ? "null" : tags.toString()) +
                ")";
    }
}
