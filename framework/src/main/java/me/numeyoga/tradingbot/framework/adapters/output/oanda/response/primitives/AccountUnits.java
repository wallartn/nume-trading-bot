package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;
import java.math.BigDecimal;

/**
 * The string representation of a quantity of an Account's home currency.
 * <p>
 * A decimal number encoded as a string. The amount of precision provided
 * depends on the Account's home currency.
 */
@JsonbTypeAdapter(AccountUnits.JsonAdapter.class)
public class AccountUnits extends StringPrimitive {

    /**
     * AccountUnits copy constructor.
     * <p>
     * @param accountUnits the 
     */
    public AccountUnits(AccountUnits accountUnits)
    {
        super(accountUnits.toString());
    }

    /**
     * AccountUnits constructor.
     * <p>
     * @param accountUnits the AccountUnits as a String
     */
    public AccountUnits(String accountUnits) {
        super(accountUnits);
    }

    /**
     * AccountUnits constructor.
     * <p>
     * @param accountUnits the AccountUnits as a double
     */
    public AccountUnits(double accountUnits) {
        super(String.valueOf(accountUnits));
    }

    /**
     * AccountUnits constructor.
     * <p>
     * @param accountUnits the AccountUnits as a BigDecimal
     */
    public AccountUnits(BigDecimal accountUnits) {
        super(accountUnits.toPlainString());
    }

    /**
     * Get the AccountUnits
     * <p>
     * The string representation of a quantity of an Account's home currency.
     * <p>
     * @return the AccountUnits as a double
     * @see AccountUnits
     */
    public double doubleValue() {
        return Double.valueOf(this.string);
    }

    /**
     * Get the AccountUnits
     * <p>
     * The string representation of a quantity of an Account's home currency.
     * <p>
     * @return the AccountUnits as a BigDecimal
     * @see AccountUnits
     */
    public BigDecimal bigDecimalValue() {
        return new BigDecimal(this.string);
    }

    /**
     * JSON adapter for reading and writing AccountUnits0
     */
    public static class JsonAdapter implements JsonbAdapter<AccountUnits, String> {

        @Override
        public String adaptToJson(AccountUnits accountUnits)
        {
            return accountUnits.toString();
        }

        @Override
        public AccountUnits adaptFromJson(String s)  {
            return new AccountUnits(s);
        }
    }
}
