package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.ClientConfigureTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * AccountConfigureResponse
 */
public class AccountConfigureResponse {

    /**
     * AccountConfigureResponse Constructor
     * <p>
     * Construct a new AccountConfigureResponse
     */
    private AccountConfigureResponse() {
    }

    @JsonbProperty("clientConfigureTransaction") private ClientConfigureTransaction clientConfigureTransaction;

    /**
     * Get the clientConfigureTransaction
     * <p>
     * The transaction that configures the Account.
     * <p>
     * @return the clientConfigureTransaction
     * @see ClientConfigureTransaction
     */
    public ClientConfigureTransaction getClientConfigureTransaction() {
        return this.clientConfigureTransaction;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the last Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
