package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.Request;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountID;

/**
 * OrderCreateRequest
 */
public class OrderCreateRequest extends Request {

    private static class Body {
        @JsonbProperty("order") private OrderRequest order;
    }

    /**
     * OrderCreateRequest Constructor
     * <p>
     * Construct a new OrderCreateRequest
     * <p>
     * @param accountID Account Identifier
     */
    public OrderCreateRequest(AccountID accountID) {
        this.body = new Body();
        this.setPathParam("accountID", accountID);

    }

    /**
     * Set the order
     * <p>
     * Specification of the Order to create
     * <p>
     * @param order the order as an OrderRequest
     * @return {@link OrderCreateRequest OrderCreateRequest}
     * @see OrderRequest
     */
    public OrderCreateRequest setOrder(OrderRequest order)
    {
        ((Body) this.body).order = order;
        return this;
    }
}
