package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.pricing_common.Price;

/**
 * PricingGetPriceRangeResponse
 */
public class PricingGetPriceRangeResponse {

    /**
     * PricingGetPriceRangeResponse Constructor
     * <p>
     * Construct a new PricingGetPriceRangeResponse
     */
    private PricingGetPriceRangeResponse() {
    }

    @JsonbProperty("prices") private ArrayList<Price> prices;

    /**
     * Get the prices
     * <p>
     * The list of prices that satisfy the request.
     * <p>
     * @return the prices
     * @see Price
     */
    public List<Price> getPrices() {
        return this.prices;
    }
}
