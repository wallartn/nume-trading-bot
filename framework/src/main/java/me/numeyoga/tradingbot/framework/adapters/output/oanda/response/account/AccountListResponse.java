package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import javax.json.bind.annotation.JsonbProperty;
import java.util.List;

public class AccountListResponse {

    @JsonbProperty("accounts")
    List<AccountProperties> accounts;

    public AccountListResponse() {
        // Nothing
    }

    /**
     * Get the accounts
     * <p>
     * The list of Accounts the client is authorized to access and their
     * associated properties.
     * <p>
     *
     * @return the accounts
     * @see AccountProperties
     */
    public List<AccountProperties> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(List<AccountProperties> accounts) {
        this.accounts = accounts;
    }
}
