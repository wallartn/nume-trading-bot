package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * AccountSummaryResponse
 */
public class AccountSummaryResponse {

    /**
     * AccountSummaryResponse Constructor
     * <p>
     * Construct a new AccountSummaryResponse
     */
    private AccountSummaryResponse() {
    }

    @JsonbProperty("account") private AccountSummary account;

    /**
     * Get the account
     * <p>
     * The summary of the requested Account.
     * <p>
     * @return the account
     * @see AccountSummary
     */
    public AccountSummary getAccount() {
        return this.account;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
