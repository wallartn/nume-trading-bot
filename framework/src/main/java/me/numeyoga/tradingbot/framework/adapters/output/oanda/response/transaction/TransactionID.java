package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The unique Transaction identifier within each Account.
 * <p>
 * String representation of the numerical OANDA-assigned TransactionID
 */
@JsonbTypeAdapter(TransactionID.JsonAdapter.class)
public class TransactionID extends StringPrimitive {

    /**
     * TransactionID copy constructor.
     * <p>
     *
     * @param transactionID the
     */
    public TransactionID(TransactionID transactionID) {
        super(transactionID.toString());
    }

    /**
     * TransactionID constructor.
     * <p>
     *
     * @param transactionID the TransactionID as a String
     */
    public TransactionID(String transactionID) {
        super(transactionID);
    }

    /**
     * JSON adapter for reading and writing TransactionID0
     */
    public static class JsonAdapter implements JsonbAdapter<TransactionID, String> {

        @Override
        public String adaptToJson(TransactionID transactionID) {
            return transactionID.toString();
        }

        @Override
        public TransactionID adaptFromJson(String s) {
            return new TransactionID(s);
        }
    }
}
