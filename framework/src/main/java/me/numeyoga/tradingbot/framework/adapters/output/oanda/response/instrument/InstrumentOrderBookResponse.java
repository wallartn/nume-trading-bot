package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.instrument;

import javax.json.bind.annotation.JsonbProperty;

/**
 * InstrumentOrderBookResponse
 */
public class InstrumentOrderBookResponse {

    /**
     * InstrumentOrderBookResponse Constructor
     * <p>
     * Construct a new InstrumentOrderBookResponse
     */
    private InstrumentOrderBookResponse() {
    }

    @JsonbProperty("orderBook") private OrderBook orderBook;

    /**
     * Get the orderBook
     * <p>
     * The instrument's order book
     * <p>
     * @return the orderBook
     * @see OrderBook
     */
    public OrderBook getOrderBook() {
        return this.orderBook;
    }
}
