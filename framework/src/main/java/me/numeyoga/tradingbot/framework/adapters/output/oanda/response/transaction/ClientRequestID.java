package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * A client provided request identifier.
 */
@JsonbTypeAdapter(ClientRequestID.JsonAdapter.class)
public class ClientRequestID extends StringPrimitive {

    /**
     * ClientRequestID copy constructor.
     * <p>
     * @param clientRequestID the 
     */
    public ClientRequestID(ClientRequestID clientRequestID)
    {
        super(clientRequestID.toString());
    }

    /**
     * ClientRequestID constructor.
     * <p>
     * @param clientRequestID the ClientRequestID as a String
     */
    public ClientRequestID(String clientRequestID) {
        super(clientRequestID);
    }

    /**
     * JSON adapter for reading and writing ClientRequestID0
     */
    public static class JsonAdapter implements JsonbAdapter<ClientRequestID, String> {

        @Override
        public String adaptToJson(ClientRequestID clientRequestID) throws Exception {
            return clientRequestID.toString();
        }

        @Override
        public ClientRequestID adaptFromJson(String s) throws Exception {
            return new ClientRequestID(s);
        }
    }
}
