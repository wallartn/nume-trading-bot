package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.RequestException;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.Transaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * OrderCreate400RequestException
 */
public class OrderCreate400RequestException extends RequestException {

    private static final long serialVersionUID = -7223082832453523433L;

    /**
     * OrderCreate400RequestException Constructor
     * <p>
     * Construct a new OrderCreate400RequestException
     */
    private OrderCreate400RequestException() {
    }

    @JsonbProperty("orderRejectTransaction") private Transaction orderRejectTransaction;

    /**
     * Get the orderRejectTransaction
     * <p>
     * The Transaction that rejected the creation of the Order as requested
     * <p>
     * @return the orderRejectTransaction
     * @see Transaction
     */
    public Transaction getOrderRejectTransaction() {
        return this.orderRejectTransaction;
    }

    @JsonbProperty("relatedTransactionIDs") private ArrayList<TransactionID> relatedTransactionIDs;

    /**
     * Get the relatedTransactionIDs
     * <p>
     * The IDs of all Transactions that were created while satisfying the
     * request.
     * <p>
     * @return the relatedTransactionIDs
     * @see TransactionID
     */
    public List<TransactionID> getRelatedTransactionIDs() {
        return this.relatedTransactionIDs;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
