package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.stream.JsonParser;
import java.lang.reflect.Type;

// GSON Deserializer for Order implementors
public class OrderAdapter implements JsonbDeserializer<Order> {

    private final Jsonb jsonb = JsonbBuilder.create();

    @Override
    public Order deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Type type) {
        JsonObject jsonObject = jsonParser.getObject();
        String typeString = jsonObject.getString("type");
        OrderType objType = OrderType.valueOf(typeString);

        switch (objType) {
        case TAKE_PROFIT:
            return jsonb.fromJson(jsonObject.toString(), TakeProfitOrder.class);
        case STOP_LOSS:
            return jsonb.fromJson(jsonObject.toString(), StopLossOrder.class);
        case TRAILING_STOP_LOSS:
            return jsonb.fromJson(jsonObject.toString(), TrailingStopLossOrder.class);
        case MARKET:
            return jsonb.fromJson(jsonObject.toString(), MarketOrder.class);
        case FIXED_PRICE:
            return jsonb.fromJson(jsonObject.toString(), FixedPriceOrder.class);
        case LIMIT:
            return jsonb.fromJson(jsonObject.toString(), LimitOrder.class);
        case STOP:
            return jsonb.fromJson(jsonObject.toString(), StopOrder.class);
        case MARKET_IF_TOUCHED:
            return jsonb.fromJson(jsonObject.toString(), MarketIfTouchedOrder.class);
        }
        return null;
    }

}
