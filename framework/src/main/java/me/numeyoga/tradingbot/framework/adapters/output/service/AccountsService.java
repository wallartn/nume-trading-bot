package me.numeyoga.tradingbot.framework.adapters.output.service;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.request.OandaBlockingClientHeadersFactory;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountGetResponse;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountListResponse;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey="accounts-api")
@RegisterClientHeaders(OandaBlockingClientHeadersFactory.class)
public interface AccountsService {

    @GET
    AccountGetResponse getById(@QueryParam("account-id") String id);

    @GET
    AccountListResponse getAll();

}
