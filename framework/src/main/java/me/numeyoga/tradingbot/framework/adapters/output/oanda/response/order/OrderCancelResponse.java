package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.OrderCancelTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * OrderCancelResponse
 */
public class OrderCancelResponse {

    /**
     * OrderCancelResponse Constructor
     * <p>
     * Construct a new OrderCancelResponse
     */
    private OrderCancelResponse() {
    }

    @JsonbProperty("orderCancelTransaction") private OrderCancelTransaction orderCancelTransaction;

    /**
     * Get the orderCancelTransaction
     * <p>
     * The Transaction that cancelled the Order
     * <p>
     * @return the orderCancelTransaction
     * @see OrderCancelTransaction
     */
    public OrderCancelTransaction getOrderCancelTransaction() {
        return this.orderCancelTransaction;
    }

    @JsonbProperty("relatedTransactionIDs") private ArrayList<TransactionID> relatedTransactionIDs;

    /**
     * Get the relatedTransactionIDs
     * <p>
     * The IDs of all Transactions that were created while satisfying the
     * request.
     * <p>
     * @return the relatedTransactionIDs
     * @see TransactionID
     */
    public List<TransactionID> getRelatedTransactionIDs() {
        return this.relatedTransactionIDs;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
