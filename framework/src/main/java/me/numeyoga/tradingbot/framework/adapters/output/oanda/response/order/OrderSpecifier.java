package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.order;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives.StringPrimitive;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * The specification of an Order as referred to by clients
 * <p>
 * Either the Order's OANDA-assigned OrderID or the Order's client-provided
 * ClientID prefixed by the "@" symbol
 */
@JsonbTypeAdapter(OrderSpecifier.JsonAdapter.class)
public class OrderSpecifier extends StringPrimitive {

    /**
     * OrderSpecifier copy constructor.
     * <p>
     * @param orderSpecifier the 
     */
    public OrderSpecifier(OrderSpecifier orderSpecifier)
    {
        super(orderSpecifier.toString());
    }

    /**
     * OrderSpecifier constructor.
     * <p>
     * @param orderSpecifier the OrderSpecifier as a String
     */
    public OrderSpecifier(String orderSpecifier) {
        super(orderSpecifier);
    }

    /**
     * Construct an OrderSpecifier from an OrderID
     * <p>
     * @param orderID the OrderID to convert
     */
    public OrderSpecifier(OrderID orderID) {
        super(orderID.toString());
    }

    /**
     * Construct an OrderSpecifier from a TransactionID
     * <p>
     * @param transactionID the TransactionID to convert
     */
    public OrderSpecifier(TransactionID transactionID) {
        super(transactionID.toString());
    }

    /**
     * JSON adapter for reading and writing OrderSpecifier0
     */
    public static class JsonAdapter implements JsonbAdapter<OrderSpecifier, String> {
        @Override
        public String adaptToJson(OrderSpecifier orderSpecifier) throws Exception {
            return orderSpecifier.toString();
        }

        @Override
        public OrderSpecifier adaptFromJson(String s) throws Exception {
            return new OrderSpecifier(s);
        }
    }
}
