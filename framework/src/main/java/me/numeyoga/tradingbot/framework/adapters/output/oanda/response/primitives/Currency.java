package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.primitives;

import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbTypeAdapter;

/**
 * Currency name identifier. Used by clients to refer to currencies.
 * <p>
 * A string containing an ISO 4217 currency
 * (http://en.wikipedia.org/wiki/ISO_4217)
 */
@JsonbTypeAdapter(Currency.JsonAdapter.class)
public class Currency extends StringPrimitive {

    /**
     * Currency copy constructor.
     * <p>
     * @param currency the 
     */
    public Currency(Currency currency)
    {
        super(currency.toString());
    }

    /**
     * Currency constructor.
     * <p>
     * @param currency the Currency as a String
     */
    public Currency(String currency) {
        super(currency);
    }

    /**
     * JSON adapter for reading and writing Currency0
     */
    public static class JsonAdapter implements JsonbAdapter<Currency, String> {
        @Override
        public String adaptToJson(Currency currency) {
            return currency.toString();
        }

        @Override
        public Currency adaptFromJson(String s) {
            return new Currency(s);
        }
    }
}
