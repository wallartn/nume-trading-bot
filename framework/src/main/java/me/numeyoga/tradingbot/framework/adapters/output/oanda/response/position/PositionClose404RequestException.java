package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.position;

import java.util.ArrayList;
import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.RequestException;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.MarketOrderRejectTransaction;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

/**
 * PositionClose404RequestException
 */
public class PositionClose404RequestException extends RequestException {

    private static final long serialVersionUID = 5116404852882287063L;

    /**
     * PositionClose404RequestException Constructor
     * <p>
     * Construct a new PositionClose404RequestException
     */
    private PositionClose404RequestException() {
    }

    @JsonbProperty("longOrderRejectTransaction") private MarketOrderRejectTransaction longOrderRejectTransaction;

    /**
     * Get the longOrderRejectTransaction
     * <p>
     * The Transaction created that rejects the creation of a MarketOrder to
     * close the long Position. Only present if the Account exists and a long
     * Position was specified.
     * <p>
     * @return the longOrderRejectTransaction
     * @see MarketOrderRejectTransaction
     */
    public MarketOrderRejectTransaction getLongOrderRejectTransaction() {
        return this.longOrderRejectTransaction;
    }

    @JsonbProperty("shortOrderRejectTransaction") private MarketOrderRejectTransaction shortOrderRejectTransaction;

    /**
     * Get the shortOrderRejectTransaction
     * <p>
     * The Transaction created that rejects the creation of a MarketOrder to
     * close the short Position. Only present if the Account exists and a short
     * Position was specified.
     * <p>
     * @return the shortOrderRejectTransaction
     * @see MarketOrderRejectTransaction
     */
    public MarketOrderRejectTransaction getShortOrderRejectTransaction() {
        return this.shortOrderRejectTransaction;
    }

    @JsonbProperty("relatedTransactionIDs") private ArrayList<TransactionID> relatedTransactionIDs;

    /**
     * Get the relatedTransactionIDs
     * <p>
     * The IDs of all Transactions that were created while satisfying the
     * request. Only present if the Account exists.
     * <p>
     * @return the relatedTransactionIDs
     * @see TransactionID
     */
    public List<TransactionID> getRelatedTransactionIDs() {
        return this.relatedTransactionIDs;
    }

    @JsonbProperty("lastTransactionID") private TransactionID lastTransactionID;

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account. Only
     * present if the Account exists.
     * <p>
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }
}
