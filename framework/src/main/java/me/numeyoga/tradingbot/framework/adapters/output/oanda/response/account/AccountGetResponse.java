package me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account;

import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.transaction.TransactionID;

import javax.json.bind.annotation.JsonbProperty;

public class AccountGetResponse {

    @JsonbProperty("account")
    Account account;
    @JsonbProperty("lastTransactionID")
    TransactionID lastTransactionID;

    public AccountGetResponse() {
        // Nothing
    }

    /**
     * Get the account
     * <p>
     * The full details of the requested Account.
     * <p>
     *
     * @return the account
     * @see Account
     */
    public Account getAccount() {
        return this.account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Get the lastTransactionID
     * <p>
     * The ID of the most recent Transaction created for the Account.
     * <p>
     *
     * @return the lastTransactionID
     * @see TransactionID
     */
    public TransactionID getLastTransactionID() {
        return this.lastTransactionID;
    }

    public void setLastTransactionID(TransactionID lastTransactionID) {
        this.lastTransactionID = lastTransactionID;
    }
}
