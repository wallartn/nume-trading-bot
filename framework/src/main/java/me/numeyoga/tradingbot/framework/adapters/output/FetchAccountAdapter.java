package me.numeyoga.tradingbot.framework.adapters.output;

import io.smallrye.common.annotation.Blocking;
import me.numeyoga.tradingbot.application.ports.output.FetchAccountOutputPort;
import me.numeyoga.tradingbot.framework.adapters.output.mapper.AccountMapper;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountGetResponse;
import me.numeyoga.tradingbot.framework.adapters.output.oanda.response.account.AccountListResponse;
import me.numeyoga.tradingbot.framework.adapters.output.service.AccountsService;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "accounts-api")
public class FetchAccountAdapter implements FetchAccountOutputPort {

    @Inject
    @RestClient
    AccountsService accountsService;

    @Inject
    AccountMapper mapper;

    @Override
    @GET
    @Path("/{account-id}")
    @Blocking
    public me.numeyoga.tradingbot.domain.account.Account fetchAccount(@PathParam("account-id") String accountId) {
        AccountGetResponse response = accountsService.getById(accountId);
        return mapper.toDomain(response.getAccount());
    }

    @GET
    @Path("/")
    @Blocking
    public AccountListResponse fetchAccounts() {
        return accountsService.getAll();
    }
}
